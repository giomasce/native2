#pragma once

#include "windowbase.h"

class GDIWindow : public WindowBase {
public:
    GDIWindow(const wchar_t *class_name, const wchar_t *name, HINSTANCE instance, BOOL quit_immediately = FALSE, HWND msg_target = 0);
    LRESULT handle(UINT uMsg, WPARAM wParam, LPARAM lParam) override;
    void initialize() override;

 private:
    HRESULT resize();
    HRESULT paint();

    BOOL quit_immediately;
    HWND edit_wnd;
    HINSTANCE instance;
    HWND msg_target;
};

#include <memory>
#include <stdexcept>

#include <giolib/main.h>

#ifndef __MINGW32__
#include <giolib/crash.h>
#include <giolib/exception.h>
#endif

#pragma comment(lib,"d3dcompiler")
#pragma comment(lib,"d3d11")
#pragma comment(lib,"windowscodecs")
#pragma comment(lib,"d2d1")
#pragma comment(lib,"ole32")
#pragma comment(lib,"dwrite")
#pragma comment(lib,"gdi32")
#pragma comment(lib,"user32")
#pragma comment(lib,"mf")
#pragma comment(lib,"mfplat")
#pragma comment(lib,"mfplay")
#pragma comment(lib,"mfreadwrite")
#pragma comment(lib,"shlwapi")
#pragma comment(lib,"mfuuid")
#pragma comment(lib,"d3d9")
#pragma comment(lib,"opengl32")
#pragma comment(lib,"usp10")
#pragma comment(lib,"d3d11")
#pragma comment(lib,"dxgi")
#pragma comment(lib,"dxguid")
//#pragma comment(lib,"mmdevapi")

#ifdef ENABLE_VULKAN
#pragma comment(lib,"vulkan-1")
#endif

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow) {
#ifndef __MINGW32__
    gio::install_crash_handler();
    gio::install_exception_handler();
    gio::install_stacktrace_handler();
#endif
    return gio::wWinMain(hInstance, hPrevInstance, pCmdLine, nCmdShow);
}

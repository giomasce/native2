#pragma once

#include <memory>

#include <windows.h>

#include "utils.h"

class WindowBase {
public:
    WindowBase(const wchar_t *class_name, const wchar_t *name, HINSTANCE instance);
    virtual ~WindowBase();
    void show(int nCmdShow);
    LRESULT default_handle(UINT uMsg, WPARAM wParam, LPARAM lParam);
    virtual LRESULT handle(UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;
    virtual void initialize();
    virtual bool idle();
    HWND get_hwnd() {
        return this->hwnd;
    }

protected:
    HWND hwnd;
};

class WindowClass {
public:
    WindowClass(HINSTANCE instance, const wchar_t *name) : instance(instance), name(name), atom{} {
        WNDCLASS wc = {};
        wc.lpfnWndProc = WindowClass::handler;
        wc.hInstance = this->instance;
        wc.lpszClassName = this->name;
        this->atom = RegisterClassW(&wc);
        MASSERT(this->atom);
    }
    ~WindowClass() {
        MASSERT(UnregisterClassW(this->name, this->instance));
    }
    template<typename T, typename... Args>
    std::shared_ptr<T> create(const wchar_t *win_name, Args&&... args) {
        auto ret = std::make_shared<T>(this->name, win_name, this->instance, std::forward<Args>(args)...);
        ret->initialize();
        return ret;
    }

private:
    static LRESULT CALLBACK handler(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
        LONG_PTR ptr = GetWindowLongPtrW(hwnd, GWLP_USERDATA);
        if (ptr) {
            auto base = reinterpret_cast<WindowBase*>(ptr);
            return base->handle(uMsg, wParam, lParam);
        }
        return DefWindowProcW(hwnd, uMsg, wParam, lParam);
    }

    HINSTANCE instance;
    const wchar_t *name;
    ATOM atom;
};

template<typename T, typename... Args>
int native2_wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR, int nCmdShow, Args&&... args) {
    WindowClass wclass(hInstance, L"Window Class");
    auto win = wclass.create<T>(L"Native2", args...);
    win->show(nCmdShow);

    MSG msg = {};
    msg.message = WM_NULL;
    bool idling = true;
    while (msg.message != WM_QUIT) {
        bool have_msg;
        if (idling) {
            have_msg = PeekMessageW(&msg, nullptr, 0, 0, PM_REMOVE);
        } else {
            GetMessageW(&msg, nullptr, 0, 0);
            have_msg = true;
        }
        if (have_msg) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            idling = true;
        } else {
            idling = win->idle();
        }
    }

    return 0;
}

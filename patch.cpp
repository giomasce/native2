#include <iostream>

#include <giolib/main.h>

#if defined(__x86_64__)
struct proc_state
{
    uint64_t rip;           /*   0 */
    uint64_t rflags;        /*   8 */
    uint64_t orig_rip;      /*  10 */
    uint64_t pad0;          /*  18 */
    uint64_t rax;           /*  20 */
    uint64_t rbx;           /*  28 */
    uint64_t rcx;           /*  30 */
    uint64_t rdx;           /*  38 */
    uint64_t rsp;           /*  40 */
    uint64_t rbp;           /*  48 */
    uint64_t rsi;           /*  50 */
    uint64_t rdi;           /*  58 */
    uint64_t r8;            /*  60 */
    uint64_t r9;            /*  68 */
    uint64_t r10;           /*  70 */
    uint64_t r11;           /*  78 */
    uint64_t r12;           /*  80 */
    uint64_t r13;           /*  88 */
    uint64_t r14;           /*  90 */
    uint64_t r15;           /*  98 */
    uint64_t xmm0[2];       /*  a0 */
    uint64_t xmm1[2];       /*  b0 */
    uint64_t xmm2[2];       /*  c0 */
    uint64_t xmm3[2];       /*  d0 */
    uint64_t xmm4[2];       /*  e0 */
    uint64_t xmm5[2];       /*  f0 */
    uint64_t xmm6[2];       /* 100 */
    uint64_t xmm7[2];       /* 110 */
    uint64_t xmm8[2];       /* 120 */
    uint64_t xmm9[2];       /* 130 */
    uint64_t xmm10[2];      /* 140 */
    uint64_t xmm11[2];      /* 150 */
    uint64_t xmm12[2];      /* 160 */
    uint64_t xmm13[2];      /* 170 */
    uint64_t xmm14[2];      /* 180 */
    uint64_t xmm15[2];      /* 190 */
    uint32_t xmcsr;         /* 1a0 */
} __attribute__((aligned(16)));
static_assert(sizeof(struct proc_state) == 0x1b0, "bad processor state size");
#define IP_NAME rip
#define ORIG_IP_NAME orig_rip
#elif defined(__i386__)
struct proc_state
{
    uint32_t eip;           /*   0 */
    uint32_t eflags;        /*   4 */
    uint32_t orig_eip;      /*   8 */
    uint32_t pad0;          /*   c */
    uint32_t eax;           /*  10 */
    uint32_t ebx;           /*  14 */
    uint32_t ecx;           /*  18 */
    uint32_t edx;           /*  1c */
    uint32_t esp;           /*  20 */
    uint32_t ebp;           /*  24 */
    uint32_t esi;           /*  28 */
    uint32_t edi;           /*  2c */
    uint64_t xmm0[2];       /*  30 */
    uint64_t xmm1[2];       /*  40 */
    uint64_t xmm2[2];       /*  50 */
    uint64_t xmm3[2];       /*  60 */
    uint64_t xmm4[2];       /*  70 */
    uint64_t xmm5[2];       /*  80 */
    uint64_t xmm6[2];       /*  90 */
    uint64_t xmm7[2];       /*  a0 */
    uint32_t xmcsr;         /*  b0 */
} __attribute__((aligned(16)));
static_assert(sizeof(struct proc_state) == 0xc0, "bad processor state size");
#define IP_NAME eip
#define ORIG_IP_NAME orig_eip
#endif

void (*__cdecl __wine_register_probe)( void *addr, void (*retaddr)( void ),
                                       void (*callback)( struct proc_state *state ) );

void
#ifdef __GNUC__
__attribute__((noinline, patchable_function_entry(8)))
#else
__declspec(noinline)
#endif
do_something() {
    std::cout << "Doing something" << std::endl;
}

static void patch(struct proc_state *state) {
#if defined(__x86_64)
    std::cout << std::hex << "In the patch! rax=" << state->rax << ", rsp=" << state->rsp << ", rip=" << state->rip << ", orig_rip=" << state->orig_rip << std::endl;
#elif defined(__i386__)
    std::cout << std::hex << "In the patch! eax=" << state->eax << ", esp=" << state->esp << ", eip=" << state->eip << ", orig_eip=" << state->orig_eip << std::endl;
#endif
}

GIO_WWINMAIN(patch) {
    HMODULE hm = LoadLibraryA("ntdll.dll");
    __wine_register_probe = reinterpret_cast<decltype(__wine_register_probe)>(GetProcAddress(hm, "__wine_register_probe"));
    std::cout << (void*)__wine_register_probe << std::endl;

    __wine_register_probe(reinterpret_cast<void*>(do_something), reinterpret_cast<void(*)()>((char*)do_something+8), patch);

    do_something();

    std::cout << "Back in main" << std::endl;

    /*void (*addr)();
#if defined(__x86_64__)
    __asm__ inline("mov %%gs:0x278, %0\n" : "=r"(addr));
    //__asm__ inline("call *%%gs:0x278\n" : : : "memory");
#elif defined(__i386__)
    __asm__ inline("mov %%fs:0x170, %0\n" : "=r"(addr));
    //__asm__ inline("call *%%fs:0x170\n" : : : "memory");
#else
#error "Architecture not supported"
#endif
    //std::cout << (void*)addr << std::endl;
    addr();*/

    return 0;
}

#include <usp10.h>

#include <giolib/main.h>

#include "utils.h"

static int uniscribe_test_main() {
    HDC hdc = GetDC(nullptr);
    SCRIPT_CACHE cache = nullptr;
    const auto str = L"gio日本語vanni";
    SCRIPT_ITEM items[17];
    int item_count;
    MASSERTHR(ScriptItemize(str, wcslen(str), 16, nullptr, nullptr, items, &item_count));
    std::cout << "Itemized " << item_count << " runs" << std::endl;
    for (int i = 0; i < item_count; i++) {
        WORD glyphs[16];
        WORD clusters[16];
        SCRIPT_VISATTR visattrs[16];
        int glyph_count;
        MASSERTHR(ScriptShape(hdc, &cache, str + items[i].iCharPos, items[i+1].iCharPos - items[i].iCharPos, 16, &items[i].a, glyphs, clusters, visattrs, &glyph_count));
        std::cout << "Shaped " << glyph_count << " glyphs:" << std::endl;
        for (int j = 0; j < glyph_count; j++) {
            std::cout << " " << glyphs[j];
        }
        std::cout << std::endl;
    }
    return 0;
}

GIO_WWINMAIN_FIRE_STD_NO_SPACE_ASSIGNMENT(uniscribe_test);

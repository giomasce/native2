#define _USE_MATH_DEFINES

#include "d3dwindow.h"

#include <stdexcept>
#include <system_error>
#include <cmath>

#include <dxgi1_3.h>

#include <glm/matrix.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <giolib/main.h>

#include "shaders.h"

D3DWindow::D3DWindow(const wchar_t *class_name, const wchar_t *name, HINSTANCE instance) : WindowBase(class_name, name, instance), frame_count(0), start_tp(std::chrono::steady_clock::now()) {
    MASSERT(SUCCEEDED(this->create_device_resources()));
    MASSERT(SUCCEEDED(this->create_device_dependent_resources()));
    MASSERT(SUCCEEDED(this->create_window_resources()));
    MASSERT(SUCCEEDED(this->create_window_size_dependent_resources()));
    SetTimer(this->hwnd, 0, 10, NULL);
}

HRESULT D3DWindow::create_device_resources() {
    D3D_FEATURE_LEVEL levels[] = {
        /*D3D_FEATURE_LEVEL_9_1,
        D3D_FEATURE_LEVEL_9_2,
        D3D_FEATURE_LEVEL_9_3,*/
        D3D_FEATURE_LEVEL_10_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_11_1
    };
    HRESULT hr = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, D3D11_CREATE_DEVICE_BGRA_SUPPORT, levels, ARRAYSIZE(levels),
                                   D3D11_SDK_VERSION, &this->device, nullptr, &this->context);
    if (FAILED(hr)) return hr;
    return S_OK;
}

HRESULT D3DWindow::create_window_resources() {
    HRESULT hr;
    Microsoft::WRL::ComPtr<IDXGIDevice3> dxgi_device;
    hr = this->device.As(&dxgi_device);
    if (FAILED(hr)) return hr;
    Microsoft::WRL::ComPtr<IDXGIAdapter> adapter;
    Microsoft::WRL::ComPtr<IDXGIFactory> factory;
    hr = dxgi_device->GetAdapter(&adapter);
    if (FAILED(hr)) return hr;
    hr = adapter->GetParent(IID_PPV_ARGS(&factory));
    if (FAILED(hr)) return hr;
    DXGI_SWAP_CHAIN_DESC desc = {};
    desc.Windowed = TRUE;
    desc.BufferCount = 2;
    desc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
    desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    desc.SampleDesc.Count = 1;
    desc.SampleDesc.Quality = 0;
    desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
    desc.OutputWindow = this->hwnd;
    hr = factory->CreateSwapChain(this->device.Get(), &desc, &this->swap_chain);
    if (FAILED(hr)) return hr;
    hr = this->configure_back_buffer();
    if (FAILED(hr)) return hr;
    return S_OK;
}

HRESULT D3DWindow::configure_back_buffer() {
    HRESULT hr;
    hr = this->swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), &this->back_buffer);
    if (FAILED(hr)) return hr;
    hr = this->device->CreateRenderTargetView(this->back_buffer.Get(), nullptr, &this->render_target);
    if (FAILED(hr)) return hr;
    this->back_buffer->GetDesc(&this->bb_desc);
    CD3D11_TEXTURE2D_DESC depth_stencil_desc(DXGI_FORMAT_D24_UNORM_S8_UINT, this->bb_desc.Width, this->bb_desc.Height, 1, 1, D3D11_BIND_DEPTH_STENCIL);
    hr = this->device->CreateTexture2D(&depth_stencil_desc, nullptr, &this->depth_stencil);
    if (FAILED(hr)) return hr;
    D3D11_DEPTH_STENCIL_VIEW_DESC depth_stencil_view_desc = {};
    depth_stencil_view_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    hr = this->device->CreateDepthStencilView(this->depth_stencil.Get(), &depth_stencil_view_desc, &this->depth_stencil_view);
    if (FAILED(hr)) return hr;
    this->viewport = {};
    this->viewport.Height = static_cast<FLOAT>(this->bb_desc.Height);
    this->viewport.Width = static_cast<FLOAT>(this->bb_desc.Width);
    this->viewport.MinDepth = 0;
    this->viewport.MaxDepth = 1;
    this->context->RSSetViewports(1, &this->viewport);
    return S_OK;
}

HRESULT D3DWindow::release_back_buffer() {
    this->render_target.Reset();
    this->back_buffer.Reset();
    this->depth_stencil_view.Reset();
    this->depth_stencil.Reset();
    this->context->Flush();
    return S_OK;
}

HRESULT D3DWindow::create_shaders() {
    HRESULT hr;
    hr = compile_vertex_shader();
    if (FAILED(hr)) return hr;
    hr = this->device->CreateVertexShader(vertex_shader_bc.data(), vertex_shader_bc.size(), nullptr, &this->vertex_shader);
    if (FAILED(hr)) return hr;
    D3D11_INPUT_ELEMENT_DESC desc[] = {
        {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
        {"COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
    };
    hr = this->device->CreateInputLayout(desc, ARRAYSIZE(desc), vertex_shader_bc.data(), vertex_shader_bc.size(), &this->input_layout);
    if (FAILED(hr)) return hr;
    hr = compile_pixel_shader();
    if (FAILED(hr)) return hr;
    hr = this->device->CreatePixelShader(pixel_shader_bc.data(), pixel_shader_bc.size(), nullptr, &this->pixel_shader);
    if (FAILED(hr)) return hr;
    CD3D11_BUFFER_DESC buffer_desc(sizeof(ConstantBufferData), D3D11_BIND_CONSTANT_BUFFER);
    hr = this->device->CreateBuffer(&buffer_desc, nullptr, &this->constant_buffer);
    if (FAILED(hr)) return hr;
    return S_OK;
}

HRESULT D3DWindow::create_cube() {
    HRESULT hr;
    VertexData vertices[] = {
        {glm::vec3(-0.5f,-0.5f,-0.5f), glm::vec3(0, 0, 0),},
        {glm::vec3(-0.5f,-0.5f, 0.5f), glm::vec3(0, 0, 1),},
        {glm::vec3(-0.5f, 0.5f,-0.5f), glm::vec3(0, 1, 0),},
        {glm::vec3(-0.5f, 0.5f, 0.5f), glm::vec3(0, 1, 1),},
        {glm::vec3( 0.5f,-0.5f,-0.5f), glm::vec3(1, 0, 0),},
        {glm::vec3( 0.5f,-0.5f, 0.5f), glm::vec3(1, 0, 1),},
        {glm::vec3( 0.5f, 0.5f,-0.5f), glm::vec3(1, 1, 0),},
        {glm::vec3( 0.5f, 0.5f, 0.5f), glm::vec3(1, 1, 1),},
    };
    CD3D11_BUFFER_DESC vertex_desc(sizeof(vertices), D3D11_BIND_VERTEX_BUFFER);
    D3D11_SUBRESOURCE_DATA vertex_data = {};
    vertex_data.pSysMem = vertices;
    vertex_data.SysMemPitch = 0;
    vertex_data.SysMemSlicePitch = 0;
    hr = this->device->CreateBuffer(&vertex_desc, &vertex_data, &this->vertex_buffer);
    if (FAILED(hr)) return hr;
    unsigned short indices[] = {
        0,2,1, // -x
        1,2,3,
        4,5,6, // +x
        5,7,6,
        0,1,5, // -y
        0,5,4,
        2,6,7, // +y
        2,7,3,
        0,4,6, // -z
        0,6,2,
        1,3,7, // +z
        1,7,5,
    };
    this->index_count = ARRAYSIZE(indices);
    CD3D11_BUFFER_DESC index_desc(sizeof(indices), D3D11_BIND_INDEX_BUFFER);
    D3D11_SUBRESOURCE_DATA index_data;
    index_data.pSysMem = indices;
    index_data.SysMemPitch = 0;
    index_data.SysMemSlicePitch = 0;
    hr = this->device->CreateBuffer(&index_desc, &index_data, &this->index_buffer);
    if (FAILED(hr)) return hr;
    return S_OK;
}

HRESULT D3DWindow::create_view_and_perspective() {
    glm::vec3 eye(0.0f, 0.7f, 1.5f);
    glm::vec3 at(0.0f,-0.1f, 0.0f);
    glm::vec3 up(0.0f, 1.0f, 0.0f);
    this->constant_buffer_data.view = glm::transpose(glm::lookAt(eye, at, up));
    this->constant_buffer_data.projection = glm::transpose(glm::perspectiveFovRH(static_cast<float>(70.0 / 180.0 * M_PI), static_cast<float>(this->bb_desc.Width), static_cast<float>(this->bb_desc.Height), 0.01f, 100.0f));
    return S_OK;
}

HRESULT D3DWindow::create_device_dependent_resources() {
    HRESULT hr;
    hr = this->create_shaders();
    if (FAILED(hr)) return hr;
    hr = this->create_cube();
    if (FAILED(hr)) return hr;
    return S_OK;
}

HRESULT D3DWindow::create_window_size_dependent_resources() {
    HRESULT hr;
    hr = this->create_view_and_perspective();
    if (FAILED(hr)) return hr;
    return S_OK;
}

HRESULT D3DWindow::update() {
    auto us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - this->start_tp).count();
    this->constant_buffer_data.world = glm::transpose(glm::eulerAngleY(static_cast<float>(us / 180.0 * M_PI / 10000.0f)));
    return S_OK;
}

HRESULT D3DWindow::render() {
    this->context->UpdateSubresource(this->constant_buffer.Get(), 0, nullptr, &this->constant_buffer_data, 0, 0);
    const float teal[] = {0.098f, 0.439f, 0.439f, 1.000f};
    this->context->ClearRenderTargetView(this->render_target.Get(), teal);
    this->context->ClearDepthStencilView(this->depth_stencil_view.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
    this->context->OMSetRenderTargets(1, this->render_target.GetAddressOf(), this->depth_stencil_view.Get());
    UINT stride = sizeof(VertexData);
    UINT offset = 0;
    this->context->IASetVertexBuffers(0, 1, this->vertex_buffer.GetAddressOf(), &stride, &offset);
    this->context->IASetIndexBuffer(this->index_buffer.Get(), DXGI_FORMAT_R16_UINT, 0);
    this->context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    this->context->IASetInputLayout(this->input_layout.Get());
    this->context->VSSetShader(this->vertex_shader.Get(), nullptr, 0);
    this->context->VSSetConstantBuffers(0, 1, this->constant_buffer.GetAddressOf());
    this->context->PSSetShader(this->pixel_shader.Get(), nullptr, 0);
    this->context->DrawIndexed(this->index_count, 0, 0);
    return S_OK;
}

HRESULT D3DWindow::present() {
    HRESULT hr;
    hr = this->swap_chain->Present(1, 0);
    if (FAILED(hr)) return hr;
    return S_OK;
}

LRESULT D3DWindow::handle(UINT uMsg, WPARAM wParam, LPARAM lParam) {
    return this->default_handle(uMsg, wParam, lParam);
}

bool D3DWindow::idle() {
    MASSERT(SUCCEEDED(this->update()));
    MASSERT(SUCCEEDED(this->render()));
    MASSERT(SUCCEEDED(this->present()));
    return false;
}

GIO_WWINMAIN(d3d) {
    return native2_wWinMain<D3DWindow>(hInstance, hPrevInstance, pCmdLine, nCmdShow);
}

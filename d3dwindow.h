#pragma once

#include <chrono>

#include <d3d11.h>
#include <wrl.h>

#include "windowbase.h"

#include <glm/glm.hpp>

class D3DWindow : public WindowBase {
public:
    D3DWindow(const wchar_t *class_name, const wchar_t *name, HINSTANCE instance);
    LRESULT handle(UINT uMsg, WPARAM wParam, LPARAM lParam) override;
    bool idle() override;

private:
    struct ConstantBufferData {
        glm::mat4x4 world;
        glm::mat4x4 view;
        glm::mat4x4 projection;
    };
    static_assert(sizeof(ConstantBufferData) % 16 == 0, "invalid size for ConstantBufferData");

    struct VertexData {
        glm::vec3 pos;
        glm::vec3 color;
    };

    HRESULT create_device_resources();
    HRESULT create_window_resources();
    HRESULT configure_back_buffer();
    HRESULT release_back_buffer();
    HRESULT create_shaders();
    HRESULT create_cube();
    HRESULT create_view_and_perspective();
    HRESULT create_device_dependent_resources();
    HRESULT create_window_size_dependent_resources();
    HRESULT update();
    HRESULT render();
    HRESULT present();

    Microsoft::WRL::ComPtr<ID3D11Device> device;
    Microsoft::WRL::ComPtr<ID3D11DeviceContext> context;
    Microsoft::WRL::ComPtr<IDXGISwapChain> swap_chain;
    Microsoft::WRL::ComPtr<ID3D11Texture2D> back_buffer;
    Microsoft::WRL::ComPtr<ID3D11RenderTargetView> render_target;
    Microsoft::WRL::ComPtr<ID3D11Texture2D> depth_stencil;
    Microsoft::WRL::ComPtr<ID3D11DepthStencilView> depth_stencil_view;
    Microsoft::WRL::ComPtr<ID3D11VertexShader> vertex_shader;
    Microsoft::WRL::ComPtr<ID3D11InputLayout> input_layout;
    Microsoft::WRL::ComPtr<ID3D11PixelShader> pixel_shader;
    Microsoft::WRL::ComPtr<ID3D11Buffer> constant_buffer;
    Microsoft::WRL::ComPtr<ID3D11Buffer> vertex_buffer;
    Microsoft::WRL::ComPtr<ID3D11Buffer> index_buffer;

    D3D11_VIEWPORT viewport;
    D3D11_TEXTURE2D_DESC bb_desc;

    ConstantBufferData constant_buffer_data;

    unsigned index_count;
    unsigned frame_count;
    std::chrono::steady_clock::time_point start_tp;
};

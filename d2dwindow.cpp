#include "d2dwindow.h"

#include <stdexcept>
#include <algorithm>

#include <giolib/main.h>

D2DWindow::D2DWindow(const wchar_t *class_name, const wchar_t *name, HINSTANCE instance) : WindowBase(class_name, name, instance) {
    if (FAILED(D2D1CreateFactory<ID2D1Factory>(D2D1_FACTORY_TYPE_SINGLE_THREADED, &this->d2d_factory))) {
        throw std::runtime_error("Cannot create D2D factory");
    }
    if (FAILED(DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), &this->dwrite_factory))) {
        throw std::runtime_error("Cannot create DWrite factory");
    }
    if (FAILED(this->dwrite_factory->CreateTextFormat(L"Stark", nullptr, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 32, L"", &this->format))) {
        throw std::runtime_error("Cannot create format");
    }
}

LRESULT D2DWindow::handle(UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg) {
    case WM_PAINT:
        MASSERT(SUCCEEDED(this->paint()));
        return 0;
    case WM_SIZE:
        MASSERT(SUCCEEDED(this->resize()));
        return 0;
    }
    return this->default_handle(uMsg, wParam, lParam);
}

HRESULT D2DWindow::create_graphics_resources() {
    HRESULT hr;
    if (!this->render_target) {
        RECT rc;
        if (!GetClientRect(this->hwnd, &rc)) return E_FAIL;
        D2D1_SIZE_U size = D2D1::SizeU(static_cast<UINT32>(rc.right), static_cast<UINT32>(rc.bottom));
        hr = this->d2d_factory->CreateHwndRenderTarget(D2D1::RenderTargetProperties(), D2D1::HwndRenderTargetProperties(this->hwnd, size), &this->render_target);
        if (FAILED(hr)) return hr;
        this->calculate_layout();
    }
    if (!this->brush) {
        const D2D1_COLOR_F color = D2D1::ColorF(1.0f, 1.0f, 0);
        hr = this->render_target->CreateSolidColorBrush(color, this->brush.ReleaseAndGetAddressOf());
        if (FAILED(hr)) return hr;
    }
    if (!this->path) {
        hr = this->d2d_factory->CreatePathGeometry(&this->path);
        if (FAILED(hr)) return hr;
        Microsoft::WRL::ComPtr<ID2D1GeometrySink> sink;
        hr = this->path->Open(&sink);
        if (FAILED(hr)) return hr;
        sink->SetFillMode(D2D1_FILL_MODE_WINDING);
        sink->BeginFigure(D2D1::Point2F(100, 50), D2D1_FIGURE_BEGIN_FILLED);
        sink->AddLine(D2D1::Point2F(150, 100));
        sink->AddQuadraticBezier(D2D1::QuadraticBezierSegment(D2D1::Point2F(100, 150), D2D1::Point2F(50, 100)));
        sink->EndFigure(D2D1_FIGURE_END_CLOSED);
        hr = sink->Close();
        if (FAILED(hr)) return hr;
    }
    if (!this->trans_path) {
        hr = this->d2d_factory->CreateTransformedGeometry(this->path.Get(), D2D1::Matrix3x2F(1, 0, 0, 1, 150, 0), &this->trans_path);
        if (FAILED(hr)) return hr;
    }
    return S_OK;
}

void D2DWindow::discard_grahics_resources() {
    this->render_target.Reset();
    this->brush.Reset();
    this->path.Reset();
}

void D2DWindow::calculate_layout() {
    if (this->render_target) {
        D2D1_SIZE_F size = this->render_target->GetSize();
        const float x = size.width / 2;
        const float y = size.height / 2;
        const float radius = std::min(x, y);
        //this->ellipse = D2D1::Ellipse(D2D1::Point2F(x, y), radius, radius);
        this->ellipse = D2D1::Ellipse(D2D1::Point2F(200, 400), 100, 50);
        this->rect = D2D1::RectF(100, 200, 300, 300);
        this->rrect = D2D1::RoundedRect(this->rect, 30, 20);
    }
}

HRESULT D2DWindow::resize() {
    if (this->render_target) {
        RECT rc;
        if (!GetClientRect(this->hwnd, &rc)) return E_FAIL;
        D2D1_SIZE_U size = D2D1::SizeU(static_cast<UINT32>(rc.right), static_cast<UINT32>(rc.bottom));
        this->render_target->Resize(size);
        this->calculate_layout();
        InvalidateRect(this->hwnd, nullptr, FALSE);
        return S_OK;
    }
    return S_FALSE;
}

static void set_point(D2D1_POINT_2F *point, float x, float y)
{
    point->x = x;
    point->y = y;
}

static void set_rect(D2D1_RECT_F *rect, float left, float top, float right, float bottom)
{
    rect->left = left;
    rect->top = top;
    rect->right = right;
    rect->bottom = bottom;
}

static void set_ellipse(D2D1_ELLIPSE *ellipse, float x, float y, float rx, float ry)
{
    set_point(&ellipse->point, x, y);
    ellipse->radiusX = rx;
    ellipse->radiusY = ry;
}

HRESULT D2DWindow::paint() {
    HRESULT hr;
    D2D1_POINT_2F p0, p1;
    D2D1_RECT_F rect;
    D2D1_ELLIPSE ellipse;

    hr = this->create_graphics_resources();
    if (FAILED(hr)) return hr;
    PAINTSTRUCT ps;
    if (!BeginPaint(this->hwnd, &ps)) return E_FAIL;

    //this->render_target->SetDpi(192, 48);
    this->render_target->SetAntialiasMode(D2D1_ANTIALIAS_MODE_ALIASED);

    this->render_target->BeginDraw();
    this->render_target->Clear(D2D1::ColorF(D2D1::ColorF::SkyBlue));
    //this->render_target->FillEllipse(this->ellipse, this->brush.Get());
    this->render_target->DrawEllipse(this->ellipse, this->brush.Get(), 10);
    //this->render_target->DrawRectangle(this->rect, this->brush.Get(), 10.0f);
    this->render_target->DrawRoundedRectangle(this->rrect, this->brush.Get(), 10.0f);
    //this->render_target->FillRoundedRectangle(this->rrect, this->brush.Get());
    this->render_target->FillGeometry(this->path.Get(), this->brush.Get());
    this->render_target->DrawGeometry(this->trans_path.Get(), this->brush.Get(), 10);

    /*set_point(&p0, 40.0f, 160.0f);
    this->render_target->DrawLine(p0, p0, this->brush.Get(), 10.0f, NULL);
    set_point(&p0, 100.0f, 160.0f);
    //set_point(&p1, 140.0f, 160.0f);
    set_point(&p1, 140.0f, 160.0f);
    this->render_target->DrawLine(p0, p1, this->brush.Get(), 10.0f, NULL);
    set_point(&p0, 200.0f,  80.0f);
    set_point(&p1, 200.0f, 240.0f);
    this->render_target->DrawLine(p0, p1, this->brush.Get(), 10.0f, NULL);
    set_point(&p0, 260.0f, 240.0f);
    set_point(&p1, 300.0f,  80.0f);
    this->render_target->DrawLine(p0, p1, this->brush.Get(), 10.0f, NULL);

    set_rect(&rect, 40.0f, 480.0f, 40.0f, 480.0f);
    this->render_target->DrawRectangle(&rect, this->brush.Get(), 10.0f, NULL);
    set_rect(&rect, 100.0f, 480.0f, 140.0f, 480.0f);
    this->render_target->DrawRectangle(&rect, this->brush.Get(), 10.0f, NULL);
    set_rect(&rect, 200.0f, 400.0f, 200.0f, 560.0f);
    this->render_target->DrawRectangle(&rect, this->brush.Get(), 10.0f, NULL);
    set_rect(&rect, 260.0f, 560.0f, 300.0f, 400.0f);
    this->render_target->DrawRectangle(&rect, this->brush.Get(), 10.0f, NULL);

    set_rect(&rect, 350.0f, 400.0f, 350.0f, 560.0f);
    this->render_target->DrawRectangle(&rect, this->brush.Get(), 10.0f, NULL);
    set_rect(&rect, 400.0f, 400.0f, 401.0f, 560.0f);
    this->render_target->DrawRectangle(&rect, this->brush.Get(), 10.0f, NULL);
    set_rect(&rect, 450.0f, 400.0f, 460.0f, 560.0f);
    this->render_target->DrawRectangle(&rect, this->brush.Get(), 10.0f, NULL);
    set_rect(&rect, 500.0f, 400.0f, 520.0f, 560.0f);
    this->render_target->DrawRectangle(&rect, this->brush.Get(), 10.0f, NULL);

    set_ellipse(&ellipse,  40.0f, 800.0f,  0.0f,  0.0f);
    this->render_target->DrawEllipse(&ellipse, this->brush.Get(), 10.0f, NULL);
    set_ellipse(&ellipse, 120.0f, 800.0f, 20.0f,  0.0f);
    this->render_target->DrawEllipse(&ellipse, this->brush.Get(), 10.0f, NULL);
    set_ellipse(&ellipse, 200.0f, 800.0f,  0.0f, 80.0f);
    this->render_target->DrawEllipse(&ellipse, this->brush.Get(), 10.0f, NULL);
    set_ellipse(&ellipse, 280.0f, 800.0f, 20.0f, 80.0f);
    this->render_target->DrawEllipse(&ellipse, this->brush.Get(), 10.0f, NULL);*/

    Microsoft::WRL::ComPtr<ID2D1PathGeometry> path2;
    hr = this->d2d_factory->CreatePathGeometry(&path2);
    if (FAILED(hr)) return hr;
    Microsoft::WRL::ComPtr<ID2D1GeometrySink> sink;
    hr = path2->Open(&sink);
    if (FAILED(hr)) return hr;
    sink->SetFillMode(D2D1_FILL_MODE_WINDING);
    sink->BeginFigure(D2D1::Point2F(400, 50), D2D1_FIGURE_BEGIN_HOLLOW);
    sink->AddQuadraticBezier(D2D1::QuadraticBezierSegment(D2D1::Point2F(425, 400), D2D1::Point2F(450, 50)));
    sink->EndFigure(D2D1_FIGURE_END_OPEN);
    hr = sink->Close();
    if (FAILED(hr)) return hr;
    this->render_target->DrawGeometry(path2.Get(), this->brush.Get(), 30);

    set_rect(&rect, 260.0f, 560.0f, 500.0f, 500.0f);
    const WCHAR string[] = L"Hello, this is a string";
    this->render_target->DrawTextW(string, sizeof(string) / sizeof(string[0]), this->format.Get(), rect, this->brush.Get());

    hr = this->render_target->EndDraw();
    if (FAILED(hr) || hr == D2DERR_RECREATE_TARGET) {
        this->discard_grahics_resources();
    }
    if (!EndPaint(this->hwnd, &ps)) return E_FAIL;
    return S_OK;
}

GIO_WWINMAIN(d2d) {
    return native2_wWinMain<D2DWindow>(hInstance, hPrevInstance, pCmdLine, nCmdShow);
}

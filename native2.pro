TEMPLATE = app
CONFIG += c++17
CONFIG += windows
CONFIG += static
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += libs/giolib libs/glm c:\VulkanSDK\1.2.170.0\Include c:\local\boost
QMAKE_LIBDIR += c:\VulkanSDK\1.2.170.0\Lib c:\local\boost\lib64-msvc-14.2
DEFINES += NOMINMAX ENABLE_OPENGL

QMAKE_CXXFLAGS += /source-charset:utf-8

SOURCES += \
        asf.cpp \
        audioclient.cpp \
        d2dwindow.cpp \
        d3dwindow.cpp \
        fonts.cpp \
        gdiwindow.cpp \
        layeredwin.cpp \
        main.cpp \
        mf.cpp \
        offscreen.cpp \
        patch.cpp \
        shaders.cpp \
        uniscribe.cpp \
        utils.cpp \
        windowbase.cpp \
        crash.cpp

HEADERS += \
    d2dwindow.h \
    d3dwindow.h \
    gdiwindow.h \
    mf.h \
    shaders.h \
    utils.h \
    windowbase.h

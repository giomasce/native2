
#include <windows.h>
#include <d3d11.h>
#include <wrl.h>
#include <DirectXMath.h>
#include <dxgi1_3.h>
#include <wincodec.h>

#include "utils.h"
#include "shaders.h"

struct VertexData {
    DirectX::XMFLOAT2 position;
    DirectX::XMFLOAT2 p0;
    DirectX::XMFLOAT2 p1;
    DirectX::XMFLOAT2 p2;
    DirectX::XMFLOAT2 prev;
    DirectX::XMFLOAT2 next;
};

struct VertexConstantBufferData {
    struct {
        float _11, _21, _31, pad0;
        float _12, _22, _32, stroke_width;
    } transform_geometry;
    DirectX::XMFLOAT4 transform_rtx;
    DirectX::XMFLOAT4 transform_rty;
};

const std::string vs_code =
R"shader(
float3x2 transform_geometry;
float stroke_width;
float4 transform_rtx;
float4 transform_rty;

struct output
{
    float2 p : WORLD_POSITION;
    float4 b : BEZIER;
    nointerpolation float2x2 stroke_transform : STROKE_TRANSFORM;
    nointerpolation float stroke_width : STROKE_WIDTH;
    nointerpolation float2x3 wtb : WTB;
    float4 position : SV_POSITION;
};

void main(float2 position : POSITION, float2 p0 : P0, float2 p1 : P1, float2 p2 : P2,
        float2 prev : PREV, float2 next : NEXT, out struct output o)
{
    float2 q_prev, q_next, v_p, q_i, p;
    float2x2 geom, rt, p_inv;
    float l;
    float a;
    float2 bc;

    geom = float2x2(transform_geometry._11_21, transform_geometry._12_22);
    rt = float2x2(transform_rtx.xy, transform_rty.xy);
    o.stroke_transform = rt * stroke_width * 0.5f;
    o.stroke_width = stroke_width;

    p = mul(geom, position);
    p0 = mul(geom, p0);
    p1 = mul(geom, p1);
    p2 = mul(geom, p2);

    float2 p12 = p1 - p2;
    float2 p10 = p1 - p0;
    float det = p12.x * p10.y - p12.y * p10.x;
    float2x2(o.wtb._11_12, o.wtb._21_22) = float2x2(p10.y, -p10.x, -p12.y, p12.x) / det;
    float2(o.wtb._13_23) = float2(1.0f, 1.0f) - mul(float2x2(o.wtb._11_12, o.wtb._21_22), p1);

    p -= p0;
    p1 -= p0;
    p2 -= p0;

    q_prev = normalize(mul(geom, prev));
    q_next = normalize(mul(geom, next));

    v_p = float2(-q_prev.y, q_prev.x);
    l = -dot(v_p, q_next) / (1.0f + dot(q_prev, q_next));
    q_i = l * q_prev + v_p;
    p += 0.5f * stroke_width * q_i;

    p_inv = float2x2(p2.y, -p2.x, -p1.y, p1.x) / (p1.x * p2.y - p2.x * p1.y);
    bc = mul(p_inv, p);
    a = 1.0f - bc.x - bc.y;
    o.b.x = a + bc.x;
    o.b.y = bc.x + bc.y;
    o.b.zw = 0.0f;

    o.p = mul(float3(position, 1.0f), transform_geometry) + 0.5f * stroke_width * q_i;
    position = mul(float2x3(transform_rtx.xyz, transform_rty.xyz), float3(o.p, 1.0f))
            * float2(transform_rtx.w, transform_rty.w);
    o.position = float4(position + float2(-1.0f, 1.0f), 0.0f, 1.0f);
}
)shader";

struct PixelConstantBufferData {
    BOOL outline;
    BOOL is_arc;
    BOOL pad0;
    BOOL pad1;
    DirectX::XMFLOAT2 curve[302];
};

const std::string ps_code =
R"shader(
bool outline;
bool is_arc;
bool pad0;
bool pad1;
float2 curve[302];

struct input
{
    float2 p : WORLD_POSITION;
    float4 b : BEZIER;
    nointerpolation float2x2 stroke_transform : STROKE_TRANSFORM;
    nointerpolation float stroke_width : STROKE_WIDTH;
    nointerpolation float2x3 wtb : WTB;
};

float4 main(struct input i) : SV_Target
{
    float4 colour;

    colour = float4(0.0f, 0.0f, 0.0f, 1.0f);
    //return colour;

    if (outline)
    {
        float2 du, dv, df;
        float4 uv;

        /* Evaluate the implicit form of the curve (u² - v = 0) in texture space,
         * using the screen-space partial derivatives to convert the calculated
         * distance to object space.
         *
         * d(x, y) = |f(x, y)| / ‖∇f(x, y)‖
         *         = |f(x, y)| / √((∂f/∂x)² + (∂f/∂y)²)
         * f(x, y) = u(x, y)² - v(x, y)
         * ∂f/∂x = 2u · ∂u/∂x - ∂v/∂x
         * ∂f/∂y = 2u · ∂u/∂y - ∂v/∂y */
        uv = i.b;
        du = float2(ddx(uv.x), ddy(uv.x));
        dv = float2(ddx(uv.y), ddy(uv.y));

        if (!is_arc) {
            df = 2.0f * uv.x * du - dv;

            clip(dot(df, uv.zw));
            clip(length(mul(i.stroke_transform, df)) - abs(uv.x * uv.x - uv.y));
        } else {
            df = 2.0f * uv.x * du + 2.0f * uv.y * dv;

            clip(dot(df, uv.zw));
            float2 uv_here = mul(i.wtb, float3(i.p, 1.0f));
            float here = uv_here.x * uv_here.x + uv_here.y * uv_here.y - 1.0f;
            //uv.xy = uv_here;
            //float here = uv.x * uv.x + uv.y * uv.y - 1.0f;
            float2 test = i.p - 0.5f * i.stroke_width * normalize(here * mul(i.stroke_transform, df));
            //test = i.p + 0.5f * i.stroke_width * float2(1.0f, -1.0f);
            float2 uv_there = mul(i.wtb, float3(test, 1.0f));
            float there = uv_there.x * uv_there.x + uv_there.y * uv_there.y - 1.0f;
            if (length(mul(i.stroke_transform, df)) - abs(uv.x * uv.x + uv.y * uv.y - 1.0f) >= 0) {
                colour += float4(0.0f, 1.0f, 0.0f, 0.0f);
            }
            if (here * there <= 0.0f) {
                colour += float4(0.0f, 0.0f, 1.0f, 0.0f);
            }
            for (int j = 0; j < 301; j++) {
                if (length(i.p - curve[j]) <= 0.5f * i.stroke_width) {
                //if (length(i.p) <= stroke_width) {
                    colour += float4(1.0f, 0.0f, 0.0f, 0.0f);
                    break;
                }
            }
            //colour = float4(uv_here, 0.0f, 1.0f);
            //colour = float4(i.p / 10, 0.0f, 1.0f);
        }
    }
    else
    {
        /* Evaluate the implicit form of the curve in texture space.
         * "i.b.z" determines which side of the curve is shaded. */
        if (!is_arc) {
            clip((i.b.x * i.b.x - i.b.y) * i.b.z);
        } else {
            clip((i.b.x * i.b.x + i.b.y * i.b.y - 1.0) * i.b.z);
        }
    }

    return colour;
}
)shader";

HWND create_window(HINSTANCE instance, const std::wstring &cname, const std::wstring &name) {
    WNDCLASS wc = {};
    wc.lpfnWndProc = DefWindowProc;
    wc.hInstance = instance;
    wc.lpszClassName = cname.c_str();
    ATOM atom = RegisterClassW(&wc);
    MASSERT(atom);

    HWND hwnd = CreateWindowExW(0, cname.c_str(), name.c_str(), WS_OVERLAPPEDWINDOW,
                                CW_USEDEFAULT, CW_USEDEFAULT, 640, 480,
                                nullptr, nullptr, instance, nullptr);
    MASSERT(hwnd);

    return hwnd;
}

inline DirectX::XMFLOAT2 diff(const DirectX::XMFLOAT2 &p1, const DirectX::XMFLOAT2 &p2) {
    DirectX::XMFLOAT2 ret;
    ret.x = p2.x - p1.x;
    ret.y = p2.y - p1.y;
    return ret;
}

inline DirectX::XMFLOAT2 neg(const DirectX::XMFLOAT2 &p) {
    DirectX::XMFLOAT2 ret;
    ret.x = -p.x;
    ret.y = -p.y;
    return ret;
}

inline FLOAT sqr(FLOAT x) {
    return x*x;
}

std::pair<std::vector<VertexData>, std::vector<DirectX::XMFLOAT2>> create_vertices() {
    const DirectX::XMFLOAT2 p0(20.0f, 20.0f);
    const DirectX::XMFLOAT2 p1(50.0f, 20.0f);
    const DirectX::XMFLOAT2 p2(50.0f, 40.0f);
    const DirectX::XMFLOAT2 r0 = diff(p1, p0);
    const DirectX::XMFLOAT2 r1 = diff(p2, p1);
    std::vector<VertexData> ret;
    ret.push_back({p0, p0, p1, p2, r0, r0});
    ret.push_back({p0, p0, p1, p2, neg(r0), neg(r0)});
    ret.push_back({p1, p0, p1, p2, r0, r1});
    ret.push_back({p2, p0, p1, p2, r1, r1});
    ret.push_back({p2, p0, p1, p2, neg(r1), neg(r1)});
    std::vector<DirectX::XMFLOAT2> ret2;
    for (FLOAT x = 20.0f; x <= 50.0f; x += 0.1f) {
        ret2.push_back({x, 40.0f - sqrtf(1.0f - sqr((x - 20.0f) / 30.0f)) * 20.0f});
    }
    MASSERT(ret2.size() == 301);
    return std::make_pair(ret, ret2);
}

void write_png(size_t width, size_t height, size_t stride, unsigned char *data) {
    HRESULT hr;
    Microsoft::WRL::ComPtr<IWICImagingFactory> factory;
    Microsoft::WRL::ComPtr<IWICBitmapEncoder> encoder;
    Microsoft::WRL::ComPtr<IWICStream> stream;
    Microsoft::WRL::ComPtr<IWICBitmapFrameEncode> frame;
    hr = CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER,
                          IID_IWICImagingFactory, &factory);
    MASSERTHR(hr);
    hr = factory->CreateEncoder(GUID_ContainerFormatPng, nullptr, &encoder);
    MASSERTHR(hr);
    hr = factory->CreateStream(&stream);
    MASSERTHR(hr);
    hr = stream->InitializeFromFilename(L"texture.png", GENERIC_WRITE);
    MASSERTHR(hr);
    hr = encoder->Initialize(stream.Get(), WICBitmapEncoderNoCache);
    MASSERTHR(hr);
    hr = encoder->CreateNewFrame(&frame, nullptr);
    MASSERTHR(hr);
    hr = frame->Initialize(nullptr);
    MASSERTHR(hr);
    hr = frame->SetSize(width, height);
    MASSERTHR(hr);
    WICPixelFormatGUID format_guid = GUID_WICPixelFormat24bppBGR;
    hr = frame->SetPixelFormat(&format_guid);
    MASSERTHR(hr);
    MASSERT(IsEqualGUID(format_guid, GUID_WICPixelFormat24bppBGR));
    hr = frame->WritePixels(height, stride, height * stride, data);
    MASSERTHR(hr);
    hr = frame->Commit();
    MASSERTHR(hr);
    hr = encoder->Commit();
    MASSERTHR(hr);
}

int WINAPI wWinMain2(HINSTANCE hInstance, HINSTANCE, PWSTR, int nCmdShow) {
    (void) hInstance;
    (void) nCmdShow;

    /*HWND hwnd = create_window(hInstance, L"Test", L"Test");
    ShowWindow(hwnd, nCmdShow);*/

    HRESULT hr;
    Microsoft::WRL::ComPtr<ID3D11Device> device;
    Microsoft::WRL::ComPtr<ID3D11DeviceContext> context;
    Microsoft::WRL::ComPtr<ID3D11RasterizerState> rast_state;
    Microsoft::WRL::ComPtr<ID3D11Texture2D> texture;
    Microsoft::WRL::ComPtr<ID3D11Texture2D> stexture;
    Microsoft::WRL::ComPtr<ID3D11RenderTargetView> render_target_view;
    Microsoft::WRL::ComPtr<ID3D11VertexShader> vertex_shader;
    Microsoft::WRL::ComPtr<ID3D11PixelShader> pixel_shader;
    Microsoft::WRL::ComPtr<ID3D11InputLayout> input_layout;
    Microsoft::WRL::ComPtr<ID3D11Buffer> vertex_buffer;
    Microsoft::WRL::ComPtr<ID3D11Buffer> index_buffer;
    Microsoft::WRL::ComPtr<ID3D11Buffer> vs_cbuf;
    Microsoft::WRL::ComPtr<ID3D11Buffer> ps_cbuf;

    hr = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED);
    MASSERTHR(hr);

    D3D_FEATURE_LEVEL levels[] = {
        D3D_FEATURE_LEVEL_11_0,
    };

    hr = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, 0, levels, ARRAYSIZE(levels),
                           D3D11_SDK_VERSION, &device, nullptr, &context);
    MASSERTHR(hr);

    /*Microsoft::WRL::ComPtr<IDXGIDevice3> dxgi_device;
    Microsoft::WRL::ComPtr<IDXGIAdapter> adapter;
    Microsoft::WRL::ComPtr<IDXGIFactory> factory;
    Microsoft::WRL::ComPtr<IDXGISwapChain> swap_chain;
    Microsoft::WRL::ComPtr<ID3D11Texture2D> back_buffer;
    hr = device.As(&dxgi_device);
    MASSERTHR(hr);
    hr = dxgi_device->GetAdapter(&adapter);
    MASSERTHR(hr);
    hr = adapter->GetParent(IID_PPV_ARGS(&factory));
    MASSERTHR(hr);
    DXGI_SWAP_CHAIN_DESC desc = {};
    desc.Windowed = TRUE;
    desc.BufferCount = 2;
    desc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
    desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    desc.SampleDesc.Count = 1;
    desc.SampleDesc.Quality = 0;
    desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
    desc.OutputWindow = hwnd;
    hr = factory->CreateSwapChain(device.Get(), &desc, &swap_chain);
    MASSERTHR(hr);
    hr = swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), &back_buffer);
    MASSERTHR(hr);*/

    D3D11_RASTERIZER_DESC rast_desc;
    rast_desc.FillMode = D3D11_FILL_SOLID;
    rast_desc.CullMode = D3D11_CULL_BACK;
    rast_desc.FrontCounterClockwise = FALSE;
    rast_desc.DepthBias = 0;
    rast_desc.DepthBiasClamp = 0.0f;
    rast_desc.SlopeScaledDepthBias = 0.0f;
    rast_desc.DepthClipEnable = TRUE;
    rast_desc.ScissorEnable = FALSE;
    rast_desc.MultisampleEnable = FALSE;
    rast_desc.AntialiasedLineEnable = FALSE;
    hr = device->CreateRasterizerState(&rast_desc, &rast_state);
    MASSERTHR(hr);

    D3D11_TEXTURE2D_DESC t2d_desc;
    t2d_desc.Width = 150;
    t2d_desc.Height = 100;
    t2d_desc.MipLevels = 1;
    t2d_desc.ArraySize = 1;
    t2d_desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    t2d_desc.SampleDesc.Count = 1;
    t2d_desc.SampleDesc.Quality = 0;
    t2d_desc.Usage = D3D11_USAGE_DEFAULT;
    t2d_desc.BindFlags = D3D11_BIND_RENDER_TARGET;
    t2d_desc.CPUAccessFlags = 0;
    t2d_desc.MiscFlags = 0;
    hr = device->CreateTexture2D(&t2d_desc, nullptr, &texture);
    MASSERTHR(hr);

    D3D11_TEXTURE2D_DESC st2d_desc = t2d_desc;
    st2d_desc.Usage = D3D11_USAGE_STAGING;
    st2d_desc.BindFlags = 0;
    st2d_desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
    hr = device->CreateTexture2D(&st2d_desc, nullptr, &stexture);
    MASSERTHR(hr);

    hr = device->CreateRenderTargetView(texture.Get(), nullptr, &render_target_view);
    MASSERTHR(hr);

    std::vector<unsigned char> vs_bc;
    std::vector<unsigned char> ps_bc;
    std::tie(hr, vs_bc, std::ignore) = compile_shader("vertex_shader", "main", "vs_4_0", vs_code);
    MASSERTHR(hr);
    hr = device->CreateVertexShader(vs_bc.data(), vs_bc.size(), nullptr, &vertex_shader);
    MASSERTHR(hr);
    std::tie(hr, ps_bc, std::ignore) = compile_shader("pixel_shader", "main", "ps_4_0", ps_code);
    MASSERTHR(hr);
    hr = device->CreatePixelShader(ps_bc.data(), ps_bc.size(), nullptr, &pixel_shader);
    MASSERTHR(hr);

    D3D11_VIEWPORT viewport;
    viewport.TopLeftX = 0;
    viewport.TopLeftY = 0;
    viewport.Width = static_cast<FLOAT>(t2d_desc.Width);
    viewport.Height = static_cast<FLOAT>(t2d_desc.Height);
    viewport.MinDepth = 0;
    viewport.MaxDepth = 1;

    D3D11_INPUT_ELEMENT_DESC il_desc[] = {
        {"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
        {"P", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 8, D3D11_INPUT_PER_VERTEX_DATA, 0},
        {"P", 1, DXGI_FORMAT_R32G32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0},
        {"P", 2, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0},
        {"PREV", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0},
        {"NEXT", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 40, D3D11_INPUT_PER_VERTEX_DATA, 0},
    };
    hr = device->CreateInputLayout(il_desc, ARRAYSIZE(il_desc), vs_bc.data(), vs_bc.size(), &input_layout);
    MASSERTHR(hr);

    auto vertices = create_vertices().first;
    D3D11_BUFFER_DESC vb_desc;
    vb_desc.ByteWidth = sizeof(decltype(vertices)::value_type) * vertices.size();
    vb_desc.Usage = D3D11_USAGE_IMMUTABLE;
    vb_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vb_desc.CPUAccessFlags = 0;
    vb_desc.MiscFlags = 0;
    vb_desc.StructureByteStride = 0;
    D3D11_SUBRESOURCE_DATA vb_data;
    vb_data.pSysMem = vertices.data();
    vb_data.SysMemPitch = 0;
    vb_data.SysMemSlicePitch = 0;
    hr = device->CreateBuffer(&vb_desc, &vb_data, &vertex_buffer);
    MASSERTHR(hr);

    unsigned int indices[] = {
        0, 2, 1,
        1, 2, 4,
        2, 3, 4,
    };
    D3D11_BUFFER_DESC ib_desc;
    ib_desc.ByteWidth = sizeof(indices);
    ib_desc.Usage = D3D11_USAGE_IMMUTABLE;
    ib_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    ib_desc.CPUAccessFlags = 0;
    ib_desc.MiscFlags = 0;
    ib_desc.StructureByteStride = 0;
    D3D11_SUBRESOURCE_DATA ib_data;
    ib_data.pSysMem = indices;
    ib_data.SysMemPitch = 0;
    ib_data.SysMemSlicePitch = 0;
    hr = device->CreateBuffer(&ib_desc, &ib_data, &index_buffer);
    MASSERTHR(hr);

    VertexConstantBufferData vscb = {
        {1.0f, 0.0f, 0.0f, 0.0f,
         0.0f, 1.0f, 0.0f, 10.0f},
        {1.0f, 0.0f, 0.0f, 2.0f / static_cast<FLOAT>(t2d_desc.Width)},
        {0.0f, 1.0f, 0.0f, -2.0f / static_cast<FLOAT>(t2d_desc.Height)},
    };
    D3D11_BUFFER_DESC vscb_desc;
    vscb_desc.ByteWidth = sizeof(vscb);
    vscb_desc.Usage = D3D11_USAGE_IMMUTABLE;
    vscb_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    vscb_desc.CPUAccessFlags = 0;
    vscb_desc.MiscFlags = 0;
    vscb_desc.StructureByteStride = 0;
    D3D11_SUBRESOURCE_DATA vscb_subres;
    vscb_subres.pSysMem = &vscb;
    vscb_subres.SysMemPitch = 0;
    vscb_subres.SysMemSlicePitch = 0;
    hr = device->CreateBuffer(&vscb_desc, &vscb_subres, &vs_cbuf);
    MASSERTHR(hr);

    PixelConstantBufferData pscb = {TRUE, TRUE, FALSE, FALSE, {}};
    auto curve = create_vertices().second;
    std::copy(curve.begin(), curve.end(), pscb.curve);
    D3D11_BUFFER_DESC pscb_desc;
    pscb_desc.ByteWidth = sizeof(pscb);
    pscb_desc.Usage = D3D11_USAGE_IMMUTABLE;
    pscb_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    pscb_desc.CPUAccessFlags = 0;
    pscb_desc.MiscFlags = 0;
    pscb_desc.StructureByteStride = 0;
    D3D11_SUBRESOURCE_DATA pscb_subres;
    pscb_subres.pSysMem = &pscb;
    pscb_subres.SysMemPitch = 0;
    pscb_subres.SysMemSlicePitch = 0;
    hr = device->CreateBuffer(&pscb_desc, &pscb_subres, &ps_cbuf);
    MASSERTHR(hr);

    FLOAT clear_color[4] = {0.2f, 0.2f, 0.2f, 0.0f};
    context->ClearRenderTargetView(render_target_view.Get(), clear_color);
    context->OMSetRenderTargets(1, render_target_view.GetAddressOf(), nullptr);
    context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    context->IASetInputLayout(input_layout.Get());
    UINT stride = sizeof(VertexData);
    UINT offset = 0;
    context->IASetVertexBuffers(0, 1, vertex_buffer.GetAddressOf(), &stride, &offset);
    context->IASetIndexBuffer(index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
    context->VSSetShader(vertex_shader.Get(), nullptr, 0);
    context->VSSetConstantBuffers(0, 1, vs_cbuf.GetAddressOf());
    context->RSSetViewports(1, &viewport);
    context->RSSetState(rast_state.Get());
    context->PSSetShader(pixel_shader.Get(), nullptr, 0);
    context->PSSetConstantBuffers(0, 1, ps_cbuf.GetAddressOf());
    context->DrawIndexed(ARRAYSIZE(indices), 0, 0);
    context->CopyResource(stexture.Get(), texture.Get());

    D3D11_MAPPED_SUBRESOURCE subres;
    hr = context->Map(stexture.Get(), 0, D3D11_MAP_READ, 0, &subres);
    MASSERTHR(hr);

    /*FILE *fout = fopen("texture.ppm", "w");
    fprintf(fout, "P3\n%d %d\n%d\n", t2d_desc.Width, t2d_desc.Height, 255);
    for (unsigned i = 0; i < t2d_desc.Height; i++) {
        const FLOAT *row = reinterpret_cast<FLOAT*>(&static_cast<BYTE*>(subres.pData)[i * subres.RowPitch]);
        for (unsigned j = 0; j < t2d_desc.Width; j++) {
            fprintf(fout, "%d %d %d\n", static_cast<int>(255.0f * row[j * 4]),
                    static_cast<int>(255.0f * row[j * 4 + 1]),
                    static_cast<int>(255.0f * row[j * 4 + 2]));
        }
    }
    fflush(fout);
    fclose(fout);*/

    std::vector<unsigned char> t8bit(t2d_desc.Width * t2d_desc.Height * 3);
    for (unsigned i = 0; i < t2d_desc.Height; i++) {
        const FLOAT *row = reinterpret_cast<FLOAT*>(&static_cast<BYTE*>(subres.pData)[i * subres.RowPitch]);
        for (unsigned j = 0; j < t2d_desc.Width; j++) {
            t8bit.at(3 * j + 3 * t2d_desc.Width * i) = static_cast<unsigned char>(255.0f * row[j * 4 + 2]);
            t8bit.at(3 * j + 3 * t2d_desc.Width * i + 1) = static_cast<unsigned char>(255.0f * row[j * 4 + 1]);
            t8bit.at(3 * j + 3 * t2d_desc.Width * i + 2) = static_cast<unsigned char>(255.0f * row[j * 4]);
        }
    }
    write_png(t2d_desc.Width, t2d_desc.Height, t2d_desc.Width * 3, t8bit.data());

    //MessageBoxW(nullptr, L"Everything's alright! :-)", L"Good!", MB_OK);

    /*MSG msg = {};
    msg.message = WM_NULL;
    bool idling = true;
    while (msg.message != WM_QUIT) {
        bool have_msg;
        if (idling) {
            have_msg = PeekMessageW(&msg, nullptr, 0, 0, PM_REMOVE);
        } else {
            GetMessageW(&msg, nullptr, 0, 0);
            have_msg = true;
        }
        if (have_msg) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            idling = true;
        } else {
            //idling = win->idle();

            FLOAT clear_color[4] = {0.3f, 0.0f, 0.0f, 0.0f};
            context->ClearRenderTargetView(render_target_view.Get(), clear_color);
            context->OMSetRenderTargets(1, render_target_view.GetAddressOf(), nullptr);
            context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
            context->IASetInputLayout(input_layout.Get());
            UINT stride = sizeof(VertexData);
            UINT offset = 0;
            context->IASetVertexBuffers(0, 1, vertex_buffer.GetAddressOf(), &stride, &offset);
            context->VSSetShader(vertex_shader.Get(), nullptr, 0);
            context->RSSetViewports(1, &viewport);
            context->PSSetShader(pixel_shader.Get(), nullptr, 0);
            context->Draw(ARRAYSIZE(vertices), 0);
            context->CopyResource(stexture.Get(), back_buffer.Get());

            hr = swap_chain->Present(1, 0);
            MASSERTHR(hr);

            idling = false;
        }
    }*/

    CoUninitialize();

    return 0;
}

#include <set>
#include <string>
#include <cstring>

#include <windows.h>
#include <d3d9.h>

#ifdef ENABLE_OPENGL
#include <gl/GL.h>
#endif

#ifdef ENABLE_VULKAN
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_win32.h>
#endif

#include <giolib/main.h>

#include "utils.h"

std::set<std::wstring> params;

void create_gdi(HWND hwnd) {
    (void)hwnd;
    std::cout << "Creating GDI surface (nothing to do)" << std::endl;
}

void paint_gdi(HWND hwnd) {
    std::cout << "Painting with GDI" << std::endl;
    HDC hdc;
    MASSERT(hdc = GetDC(hwnd));
    HBRUSH brush = (HBRUSH)GetStockObject(WHITE_BRUSH);
    MASSERT(brush);
    RECT rect = { 0, 0, 600, 600 };
    //MASSERT(FillRect(hdc, &rect, brush));
    brush = (HBRUSH)GetStockObject(GRAY_BRUSH);
    MASSERT(brush);
    rect = { 50, 50, 100, 100 };
    MASSERT(FillRect(hdc, &rect, brush));
    MASSERT(ReleaseDC(hwnd, hdc));
}

IDirect3D9 *d3d9 = nullptr;
IDirect3DDevice9 *d3d9_dev = nullptr;

void create_d3d9(HWND hwnd) {
    std::cout << "Creating D3D9 surface" << std::endl;
    if (!d3d9) {
        MASSERT(d3d9 = Direct3DCreate9(D3D_SDK_VERSION));
    }
    if (!d3d9_dev) {
        D3DPRESENT_PARAMETERS d3dpp{};
        d3dpp.Windowed = TRUE;
        d3dpp.SwapEffect = D3DSWAPEFFECT_COPY;
        HRESULT hr = d3d9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hwnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &d3d9_dev);
        MASSERTHR(hr);
        MASSERT(d3d9_dev);
    }
}

void paint_d3d9(HWND hwnd) {
    (void)hwnd;
    if (!d3d9_dev) {
        std::cout << "Cannot paint with D3D9" << std::endl;
        return;
    }
    std::cout << "Painting with D3D9" << std::endl;
    MASSERTHR(d3d9_dev->Clear(0, nullptr, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 40, 100), 1.0f, 0));
    MASSERTHR(d3d9_dev->BeginScene());
    MASSERTHR(d3d9_dev->EndScene());
    MASSERTHR(d3d9_dev->Present(nullptr, nullptr, 0, nullptr));
}

#ifdef ENABLE_OPENGL
HDC hgldc = nullptr;
HGLRC hglrc = nullptr;

void create_opengl(HWND hwnd) {
    std::cout << "Create OpenGL surface" << std::endl;
    if (!hgldc) {
        hgldc = GetDC(hwnd);
        MASSERT(hgldc);
    }
    if (!hglrc) {
        PIXELFORMATDESCRIPTOR pfd{};
        pfd.nSize = sizeof(pfd);
        pfd.nVersion = 1;
        pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
        pfd.iPixelType = PFD_TYPE_RGBA;
        pfd.cColorBits = 32;
        int pf = ChoosePixelFormat(hgldc, &pfd);
        MASSERT(pf);
        MASSERT(SetPixelFormat(hgldc, pf, &pfd));
        hglrc = wglCreateContext(hgldc);
        MASSERT(hglrc);
    }
}

void paint_opengl(HWND hwnd) {
    (void)hwnd;
    if (!hglrc) {
        std::cout << "Cannot paint with OpenGL" << std::endl;
        return;
    }
    std::cout << "Drawing with OpenGL" << std::endl;
    wglMakeCurrent(hgldc, hglrc);
    //glClear(GL_COLOR_BUFFER_BIT);
    glBegin(GL_TRIANGLES);
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex2i(0,  1);
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex2i(-1, -1);
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex2i(1, -1);
    glEnd();
    glFlush();
    wglMakeCurrent(nullptr, nullptr);
}
#endif

void set_layered_attributes(HWND hwnd, int alpha) {
    std::cout << "Setting layered window attributes" << std::endl;
    MASSERT(SetLayeredWindowAttributes(hwnd, RGB(128, 128, 128), alpha, LWA_ALPHA | LWA_COLORKEY));
}

void update_layered(HWND hwnd) {
    std::cout << "Updating layered window" << std::endl;
    HDC screen_dc = GetDC(0);
    MASSERT(screen_dc);
    HDC win_dc = GetDC(hwnd);
    MASSERT(win_dc);
    HDC bitmap_dc = CreateCompatibleDC(win_dc);
    MASSERT(bitmap_dc);
    /*HBITMAP bitmap = CreateCompatibleBitmap(win_dc, 600, 600);
      MASSERT(bitmap);*/
    BITMAPINFOHEADER bih{};
    bih.biSize = sizeof(bih);
    bih.biWidth = 600;
    bih.biHeight = 600;
    bih.biPlanes = 1;
    bih.biBitCount = 32;
    bih.biCompression = BI_RGB;
    DWORD *bits;
    HBITMAP bitmap = CreateDIBSection(win_dc, reinterpret_cast<BITMAPINFO*>(&bih), DIB_RGB_COLORS, reinterpret_cast<void**>(&bits), nullptr, 0);
    MASSERT(bitmap);
    MASSERT(SelectObject(bitmap_dc, bitmap));
    //memset(bits, 128, 600 * 600 * 4);
    for (int x = 0; x < 600; x++) {
        for (int y = 0; y < 600; y++) {
            bits[600*y+x] = 0x80008000;
        }
    }
    for (int x = 400; x < 500; x++) {
        for (int y = 100; y < 200; y++) {
            bits[600*y+x] = 0xff800000;
        }
    }
    for (int x = 550; x < 600; x++) {
        for (int y = 0; y < 50; y++) {
            bits[600*y+x] = 0xff0000ff;
        }
    }

    /*HBRUSH brush = (HBRUSH)GetStockObject(WHITE_BRUSH);
    MASSERT(brush);
    RECT rect = { 0, 0, 600, 600 };
    MASSERT(FillRect(bitmap_dc, &rect, brush));
    brush = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
    MASSERT(brush);
    rect = { 50, 50, 100, 100 };
    MASSERT(FillRect(bitmap_dc, &rect, brush));*/

    POINT origin{0, 0};
    SIZE size{600, 600};
    BLENDFUNCTION blend{};
    blend.BlendOp = AC_SRC_OVER;
    blend.SourceConstantAlpha = 255;
    blend.AlphaFormat = AC_SRC_ALPHA;
    MASSERT(UpdateLayeredWindow(hwnd, screen_dc, &origin, &size, bitmap_dc, &origin, 0, &blend, ULW_ALPHA));

    DeleteObject(bitmap);
    DeleteDC(bitmap_dc);
    ReleaseDC(hwnd, win_dc);
    ReleaseDC(0, screen_dc);
}

static LRESULT CALLBACK handler(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg) {
    case WM_CLOSE:
        std::cout << "quitting" << std::endl;
        PostQuitMessage(0);
        break;
    case WM_CHAR:
        std::wcout << "character: " << (WCHAR)wParam << std::endl;
        switch ((WCHAR)wParam) {
        case 'q':
            std::cout << "quitting" << std::endl;
            PostQuitMessage(0);
            break;
        case 'G': create_gdi(hwnd); break;
        case 'g': paint_gdi(hwnd); break;
        case 'D': create_d3d9(hwnd); break;
        case 'd': paint_d3d9(hwnd); break;
#ifdef ENABLE_OPENGL
        case 'O': create_opengl(hwnd); break;
        case 'o': paint_opengl(hwnd); break;
#endif
        case 'L': set_layered_attributes(hwnd, 200); break;
        case 'K': set_layered_attributes(hwnd, 100); break;
        case 'l': update_layered(hwnd); break;
        }
        break;
    case WM_PAINT:
        std::cout << "WM_PAINT" << std::endl;
        //paint_gdi(hwnd);
        break;
    default:
        //std::cout << "message " << uMsg << std::endl;
	break;
    }
    return DefWindowProcW(hwnd, uMsg, wParam, lParam);
}

GIO_WWINMAIN(layered) {
    (void)hPrevInstance;
    WNDCLASS wc = {};
    wc.lpfnWndProc = handler;
    wc.hInstance = hInstance;
    wc.lpszClassName = L"Window class";
    ATOM atom = RegisterClassW(&wc);
    MASSERT(atom);

    while (pCmdLine && pCmdLine[0]) {
        WCHAR *pos = wcschr(pCmdLine, ' ');
        if (pos) {
            params.emplace(pCmdLine, pos);
            pCmdLine = pos + 1;
        } else {
            params.emplace(pCmdLine);
            pCmdLine = nullptr;
        }
    }

    DWORD ex_flags = 0;
    DWORD flags = 0;
    if (params.find(L"layered") != std::end(params)) {
        ex_flags |= WS_EX_LAYERED;
    }
    if (params.find(L"popup") != std::end(params)) {
        flags |= WS_POPUP;
    } else {
        flags |= WS_OVERLAPPED;
    }
    HWND hwnd = CreateWindowExW(ex_flags, L"Window class", L"Window name", flags, 100, 100, 600, 600, nullptr, nullptr, hInstance, nullptr);
    ShowWindow(hwnd, nCmdShow);

#ifdef ENABLE_VULKAN
    if (params.find(L"vulkan") != std::end(params)) {
        std::cout << "Creating vulkan surface" << std::endl;
        static const char *extensions[] = { "VK_KHR_surface", "VK_KHR_win32_surface" };
        VkApplicationInfo app_info{};
        app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        app_info.pApplicationName = "Layered window with Vulkan";
        app_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        app_info.pEngineName = "No Engine";
        app_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        app_info.apiVersion = VK_API_VERSION_1_0;
        VkInstanceCreateInfo create_info{};
        create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        create_info.pApplicationInfo = &app_info;
        create_info.enabledExtensionCount = sizeof(extensions) / sizeof(extensions[0]);
        create_info.ppEnabledExtensionNames = extensions;
        VkInstance instance;
        VkResult result = vkCreateInstance(&create_info, nullptr, &instance);
        MASSERT(result == VK_SUCCESS);

        VkWin32SurfaceCreateInfoKHR win_info{};
        win_info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
        win_info.hinstance = hInstance;
        win_info.hwnd = hwnd;
        VkSurfaceKHR surface;
        result = vkCreateWin32SurfaceKHR(instance, &win_info, nullptr, &surface);
        MASSERT(result == VK_SUCCESS);
    }
#endif

    MSG msg = {};
    msg.message = WM_NULL;
    while (msg.message != WM_QUIT) {
        GetMessageW(&msg, nullptr, 0, 0);
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

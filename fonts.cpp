#include <iostream>
#include <set>
#include <string>

#include <windows.h>
#include <dwrite.h>

#include <giolib/main.h>

#include "utils.h"

static void print_fonts(const std::set<std::wstring> &fonts) {
    std::wcout << "There are " << fonts.size() << " fonts:\n";
    for (const auto &font : fonts) {
        std::wcout << font << '\n';
        std::wcout.clear();
    }
    std::wcout << "Do we have Stark? " << (fonts.find(L"Stark") != fonts.end() ? "YES!" : "no...") << '\n';
}

static int CALLBACK enum_fonts_cb(const LOGFONT *elfe, const TEXTMETRIC *ntme, DWORD type, LPARAM self) {
    auto &fonts = *reinterpret_cast<std::set<std::wstring>*>(self);
    fonts.insert(elfe->lfFaceName);
    return 1;
}

GIO_WWINMAIN_NOARGS(gdi_enum_fonts) {
    HDC hdc = GetDC(nullptr);
    MASSERT(hdc);
    LOGFONT query;
    query.lfCharSet = DEFAULT_CHARSET;
    query.lfFaceName[0] = 0;
    query.lfPitchAndFamily = 0;
    std::set<std::wstring> fonts;
    EnumFontFamiliesExW(hdc, &query, enum_fonts_cb, reinterpret_cast<LPARAM>(&fonts), 0);

    print_fonts(fonts);

    return 0;
}

GIO_WWINMAIN_NOARGS(gdi_add_font) {
    int res = AddFontResourceA("Stark.OTF");
    if (res != 1) {
        throw std::runtime_error("cannot add font");
    }
    return 0;
}

GIO_WWINMAIN_NOARGS(gdi_remove_font) {
    int res = RemoveFontResourceA("Stark.OTF");
    if (res != 1) {
        throw std::runtime_error("cannot remove font");
    }
    return 0;
}

void dwrite_enum_fonts(DWRITE_FACTORY_TYPE factory_type, BOOL update_collection) {
    HRESULT hr;

    IDWriteFactory *factory;
    hr = DWriteCreateFactory(factory_type, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown**>(&factory));
    MASSERT(hr == S_OK);

    IDWriteFontCollection *collection;
    hr = factory->GetSystemFontCollection(&collection, update_collection);
    MASSERT(hr == S_OK);

    std::set<std::wstring> fonts;
    for (UINT32 i = 0; i < collection->GetFontFamilyCount(); i++) {
        IDWriteFontFamily *family;
        hr = collection->GetFontFamily(i, &family);
        MASSERT(hr == S_OK);

        IDWriteLocalizedStrings *names;
        hr = family->GetFamilyNames(&names);
        MASSERT(hr == S_OK);

        for (UINT32 j = 0; j < names->GetCount(); j++) {
            UINT32 len;
            hr = names->GetStringLength(j, &len);
            MASSERT(hr == S_OK);

            std::vector<wchar_t> buf(len+1);
            hr = names->GetString(j, buf.data(), len+1);
            MASSERT(hr == S_OK);
            std::wstring str(buf.data());
            fonts.insert(str);
        }

        names->Release();
        family->Release();
    }

    collection->Release();
    factory->Release();

    print_fonts(fonts);
}

GIO_WWINMAIN_NOARGS(dwrite_enum_shared_fonts) {
    dwrite_enum_fonts(DWRITE_FACTORY_TYPE_SHARED, FALSE);
    return 0;
}

GIO_WWINMAIN_NOARGS(dwrite_enum_isolated_fonts) {
    dwrite_enum_fonts(DWRITE_FACTORY_TYPE_ISOLATED, FALSE);
    return 0;
}

GIO_WWINMAIN_NOARGS(dwrite_enum_shared_fonts_update) {
    dwrite_enum_fonts(DWRITE_FACTORY_TYPE_SHARED, TRUE);
    return 0;
}

GIO_WWINMAIN_NOARGS(dwrite_enum_isolated_fonts_update) {
    dwrite_enum_fonts(DWRITE_FACTORY_TYPE_ISOLATED, TRUE);
    return 0;
}

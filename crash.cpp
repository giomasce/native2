#include <giolib/main.h>

GIO_WWINMAIN_NOARGS(crash_segfault) {
    volatile int *ptr = nullptr;
    *ptr = 0;
    return 0;
}

GIO_WWINMAIN_NOARGS(crash_abort) {
    abort();
}

#ifndef _MSVC_LANG
GIO_WWINMAIN_NOARGS(crash_illegal) {
    __asm__ volatile("ud2\n");
    return 0;
}

GIO_WWINMAIN_NOARGS(crash_breakpoint) {
    __asm__ volatile("int3\n");
    return 0;
}
#endif

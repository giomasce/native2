#include <iostream>
#include <set>
#include <mutex>
#include <array>
#include <chrono>
#include <iomanip>
#include <fstream>
#include <thread>

#include <wrl.h>
#include <mfidl.h>
#include <mferror.h>
#include <mfapi.h>
#include <mfreadwrite.h>
#include <shlwapi.h>
#include <propvarutil.h>
#include <mfplay.h>
#include <d3d11.h>

#include <giolib/main.h>
#include <giolib/encoding.h>

#include "utils.h"
#include "windowbase.h"

class callback_t final : public IMFSourceReaderCallback {
public:
    static HRESULT CreateInstance(HANDLE event, callback_t **ptr) {
        if (!ptr) {
            return E_POINTER;
        }
        callback_t *callback = new(std::nothrow) callback_t(event);
        if (!callback) {
            return E_OUTOFMEMORY;
        }
        *ptr = callback;
        std::cout << "Create " << *ptr << std::endl;
        return S_OK;
    }

    void set_reader(Microsoft::WRL::ComPtr<IMFSourceReader> reader) {
        this->reader = reader;
    }

    STDMETHODIMP QueryInterface(REFIID riid, void **ptr) {
        static const QITAB qit[] = {
            QITABENT(callback_t, IMFSourceReaderCallback),
            {0, 0},
        };
        return QISearch(this, qit, riid, ptr);
    }

    STDMETHODIMP_(ULONG) AddRef() {
        ULONG count = InterlockedIncrement(&this->refcount);
        std::cout << "AddRef to " << count << std::endl;
        return count;
    }

    STDMETHODIMP_(ULONG) Release() {
        ULONG count = InterlockedDecrement(&this->refcount);
        std::cout << "Release to " << count << std::endl;
        if (count == 0) {
            std::cout << "Destroy " << this << std::endl;
            delete this;
        }
        return count;
    }

    STDMETHODIMP OnEvent(DWORD stream_idx, IMFMediaEvent *event) {
        std::cout << "OnEvent " << stream_idx << " " << event << std::endl;
        SetEvent(this->event);
        return S_OK;
    }

    STDMETHODIMP OnFlush(DWORD stream_idx) {
        std::cout << "OnFlush " << stream_idx << std::endl;
        SetEvent(this->event);
        return S_OK;
    }

    STDMETHODIMP OnReadSample(HRESULT status, DWORD stream_idx, DWORD stream_flags, LONGLONG timestamp, IMFSample *sample) {
        std::lock_guard guard(this->mutex);
        std::cout << "OnReadSample " << status << " " << stream_idx << " " << stream_flags << " " << timestamp << " " << sample << std::endl;
        if (!(stream_flags & MF_SOURCE_READERF_ENDOFSTREAM)) {
            std::cout << "Read another sample" << std::endl;
            MASSERTHR(this->reader->ReadSample(stream_idx, 0, nullptr, nullptr, nullptr, nullptr));
            std::cout << "Request issued" << std::endl;
        } else {
            this->finished.insert(stream_idx);
            SetEvent(this->event);
        }
        if (this->finished.size() == 2) {
            //SetEvent(this->event);
        }
        return S_OK;
    }

protected:
    callback_t(HANDLE event) : refcount(1), event(event) {
    }

    virtual ~callback_t() {
    }

    ULONG refcount;
    HANDLE event;
    Microsoft::WRL::ComPtr<IMFSourceReader> reader;
    std::set<size_t> finished;
    std::mutex mutex;
};

void dump_propvar(const PROPVARIANT &var) {
    std::cout << "{VT=" << std::dec << var.vt << ' ';
    switch (var.vt) {
    case VT_UI4: std::cout << var.ulVal; break;
    case VT_I4: std::cout << var.lVal; break;
    case VT_UI8: std::cout << var.uhVal.QuadPart; break;
    case VT_I8: std::cout << var.hVal.QuadPart; break;
    case VT_LPWSTR: std::wcout << var.pwszVal; break;
    case VT_CLSID: {
        WCHAR *tmp;
        StringFromCLSID(*var.puuid, &tmp);
        std::wcout << tmp;
        CoTaskMemFree(tmp);
        break; }
    }
    std::cout << "}";
}

void dump_media_type(IMFMediaType *media_type) {
    PROPVARIANT pvar;
    HRESULT hr;

    std::cout << "Dumping media type:" << std::endl;

#define DUMP(x) \
    hr = media_type->GetItem(x, &pvar); \
    if (SUCCEEDED(hr)) { \
        std::cout << " * " #x ": "; \
        dump_propvar(pvar); \
        std::cout << std::endl; \
    }

    DUMP(MF_MT_MAJOR_TYPE)
    DUMP(MF_MT_SUBTYPE)
    DUMP(MF_MT_AUDIO_BITS_PER_SAMPLE)
    DUMP(MF_MT_AUDIO_SAMPLES_PER_SECOND)
    DUMP(MF_MT_AUDIO_NUM_CHANNELS)
    DUMP(MF_MT_AUDIO_CHANNEL_MASK)
    DUMP(MF_MT_FRAME_SIZE)
    DUMP(MF_MT_FRAME_RATE)
    DUMP(MF_MT_VIDEO_ROTATION)

    PropVariantClear(&pvar);
}

static void do_stuff_with_source_reader(Microsoft::WRL::ComPtr<IMFMediaSource> media_source) {
    Microsoft::WRL::ComPtr<callback_t> callback;
    Microsoft::WRL::ComPtr<IUnknown> unk_callback;
    Microsoft::WRL::ComPtr<IMFAttributes> attrs;
    Microsoft::WRL::ComPtr<IMFSourceReader> reader;
    Microsoft::WRL::ComPtr<IMFMediaTypeHandler> media_type_handler;
    Microsoft::WRL::ComPtr<IMFMediaType> media_type;
    Microsoft::WRL::ComPtr<IMFMediaType> first_media_type;
    PROPVARIANT var;
    HRESULT hr;

    std::cout << "Create event" << std::endl;
    HANDLE event = CreateEvent(nullptr, FALSE, FALSE, nullptr);

    std::cout << "Create callback" << std::endl;
    MASSERTHR(callback_t::CreateInstance(event, &callback));
    std::cout << "Create attributes" << std::endl;
    MASSERTHR(MFCreateAttributes(&attrs, 1));
    MASSERTHR(callback.As<IUnknown>(&unk_callback));
    std::cout << "Set callback attribute" << std::endl;
    MASSERTHR(attrs->SetUnknown(MF_SOURCE_READER_ASYNC_CALLBACK, unk_callback.Get()));
    std::cout << "Create source reader" << std::endl;
    MASSERTHR(MFCreateSourceReaderFromMediaSource(media_source.Get(), attrs.Get(), &reader));
    callback->set_reader(reader);

    for (int j = 0; ; j++) {
        std::cout << "GetCurrentMediaType " << std::dec << j << std::endl;
        hr = reader->GetCurrentMediaType(j, &media_type);
        std::cout << "returned " << std::hex << hr << std::endl;
        if (j == 2)
            break;
        dump_media_type(media_type.Get());

        std::cout << "GetPresentationAttribute " << std::dec << j << " MF_SD_PROTECTED" << std::endl;
        hr = reader->GetPresentationAttribute(j, MF_SD_PROTECTED, &var);
        std::cout << "returned " << std::hex << hr << std::endl;
        PropVariantClear(&var);

        for (int i = 0; ; i++) {
            std::cout << "GetNativeMediaType " << std::dec << j << ' ' << i << std::endl;
            hr = reader->GetNativeMediaType(j, i, &media_type);
            std::cout << "returned " << std::hex << hr << std::endl;
            if (i == 0)
                first_media_type = media_type;
            if (hr == MF_E_NO_MORE_TYPES)
                break;
            dump_media_type(media_type.Get());
        }

        std::cout << "SetCurrentMediaType " << std::dec << j << std::endl;
        hr = reader->SetCurrentMediaType(j, nullptr, first_media_type.Get());
        std::cout << "returned " << std::hex << hr << std::endl;

        std::cout << "GetPresentationAttribute " << std::dec << j << " MF_SD_LANGUAGE" << std::endl;
        hr = reader->GetPresentationAttribute(j, MF_SD_LANGUAGE, &var);
        std::cout << "returned " << std::hex << hr << std::endl;
        dump_propvar(var);
        std::cout << std::endl;
        PropVariantClear(&var);

        std::cout << "GetPresentationAttribute " << std::dec << j << " MF_SD_STREAM_NAME" << std::endl;
        hr = reader->GetPresentationAttribute(j, MF_SD_STREAM_NAME, &var);
        std::cout << "returned " << std::hex << hr << std::endl;
        dump_propvar(var);
        std::cout << std::endl;
        PropVariantClear(&var);
    }

    for (int i = 0; i < 2; i++) {
        std::cout << "SetStreamSelection " << std::dec << i << std::endl;
        hr = reader->SetStreamSelection(i, TRUE);
        std::cout << "returned " << std::hex << hr << std::endl;
    }

    std::cout << "GetPresentationAttribute " << std::dec << -1 << " MF_PD_DURATION: ";
    hr = reader->GetPresentationAttribute(-1, MF_PD_DURATION, &var);
    dump_propvar(var);
    std::cout << std::endl;
    PropVariantClear(&var);
    std::cout << "returned " << std::hex << hr << std::endl;

    std::cout << "Flush" << std::endl;
    MASSERTHR(reader->Flush(MF_SOURCE_READER_ALL_STREAMS));
    std::cout << "Set current position: ";
    InitPropVariantFromInt64(0, &var);
    dump_propvar(var);
    std::cout << std::endl;
    MASSERTHR(reader->SetCurrentPosition(GUID_NULL, var));
    PropVariantClear(&var);
    std::cout << "Wait for flush" << std::endl;
    WaitForSingleObject(event, INFINITE);
    std::cout << "Flush again" << std::endl;
    MASSERTHR(reader->Flush(MF_SOURCE_READER_ALL_STREAMS));
    std::cout << "Wait again for flush" << std::endl;
    WaitForSingleObject(event, INFINITE);
    std::cout << "Read a sample from first stream" << std::endl;
    MASSERTHR(reader->ReadSample(0, 0, nullptr, nullptr, nullptr, nullptr));
    std::cout << "Read a sample from second stream" << std::endl;
    MASSERTHR(reader->ReadSample(1, 0, nullptr, nullptr, nullptr, nullptr));
    std::cout << "Wait for sample" << std::endl;
    WaitForSingleObject(event, INFINITE);
    //media_source->Shutdown();
    //Sleep(1000);
    std::cout << "Finished!" << std::endl;
}

static void do_stuff_with_media_source(Microsoft::WRL::ComPtr<IMFMediaSource> media_source) {
    Microsoft::WRL::ComPtr<IMFPresentationDescriptor> presentation_descr;
    DWORD stream_num;

    std::cout << "Create presentation descriptor" << std::endl;
    MASSERTHR(media_source->CreatePresentationDescriptor(&presentation_descr));
    std::cout << "Get stream number" << std::endl;
    MASSERTHR(presentation_descr->GetStreamDescriptorCount(&stream_num));
    for (DWORD i = 0; i < stream_num; i++) {
        Microsoft::WRL::ComPtr<IMFStreamDescriptor> stream_descr;
        Microsoft::WRL::ComPtr<IMFMediaTypeHandler> media_type_handler;
        Microsoft::WRL::ComPtr<IMFMediaType> media_type;
        BOOL selected;

        std::cout << "Get stream descriptor" << std::endl;
        MASSERTHR(presentation_descr->GetStreamDescriptorByIndex(i, &selected, &stream_descr));
        std::cout << "Get media type handler" << std::endl;
        MASSERTHR(stream_descr->GetMediaTypeHandler(&media_type_handler));

        DWORD media_type_count;
        std::cout << "Get media type count" << std::endl;
        MASSERTHR(media_type_handler->GetMediaTypeCount(&media_type_count));
        for (DWORD j = 0; j < media_type_count; j++) {
            std::cout << "Get media type " << j << std::endl;
            MASSERTHR(media_type_handler->GetMediaTypeByIndex(j, &media_type));
            dump_media_type(media_type.Get());
        }

        std::cout << "Get current media type" << std::endl;
        MASSERTHR(media_type_handler->GetCurrentMediaType(&media_type));
        dump_media_type(media_type.Get());

        GUID guid;
        MASSERTHR(media_type->GetGUID(MF_MT_MAJOR_TYPE, &guid));
        if (guid == MFMediaType_Audio) {
            std::cout << "Mangling settings" << std::endl;
            MASSERTHR(media_type->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, 1000));
            MASSERTHR(media_type_handler->IsMediaTypeSupported(media_type.Get(), nullptr));
            MASSERTHR(media_type_handler->SetCurrentMediaType(media_type.Get()));

            std::cout << "Get current media type" << std::endl;
            MASSERTHR(media_type_handler->GetCurrentMediaType(&media_type));
            dump_media_type(media_type.Get());
        }
    }

    PROPVARIANT pvar;
    PropVariantInit(&pvar);
    MASSERTHR(media_source->Start(presentation_descr.Get(), nullptr, &pvar));
}

static void do_stuff(const std::wstring &filename) {
    Microsoft::WRL::ComPtr<IMFByteStream> byte_stream;
    Microsoft::WRL::ComPtr<IMFSourceResolver> resolver;
    Microsoft::WRL::ComPtr<IUnknown> unk_media_source;
    Microsoft::WRL::ComPtr<IMFMediaSource> media_source;

    std::cout << "Create byte stream" << std::endl;
    MASSERTHR(MFCreateFile(MF_ACCESSMODE_READ, MF_OPENMODE_FAIL_IF_NOT_EXIST, MF_FILEFLAGS_NONE,
                           filename.c_str(), &byte_stream));
    std::cout << "Create resolver" << std::endl;
    MASSERTHR(MFCreateSourceResolver(&resolver));
    MF_OBJECT_TYPE type;
    std::cout << "Create media source" << std::endl;
    MASSERTHR(resolver->CreateObjectFromByteStream(byte_stream.Get(), filename.c_str(), MF_RESOLUTION_MEDIASOURCE,
                                                   nullptr, &type, &unk_media_source));
    MASSERTHR(unk_media_source.As<IMFMediaSource>(&media_source));

    if (false) {
        do_stuff_with_source_reader(media_source);
    } else {
        do_stuff_with_media_source(media_source);
    }
}

GIO_WWINMAIN(mfplat) {
    (void)hInstance;
    (void)hPrevInstance;
    (void)nCmdShow;
    MASSERTHR(MFStartup(MF_VERSION, MFSTARTUP_FULL));
    do_stuff(pCmdLine);
    MASSERTHR(MFShutdown());
    return 0;
}

static const char *resolve_type(MediaEventType type) {
    switch (type) {
#define CASE(x) case x: return #x;
    CASE(MESourceStarted)
    CASE(MEStreamStarted)
    CASE(MEStreamSinkStarted)
    CASE(MESourceStopped)
    CASE(MEStreamStopped)
    CASE(MEStreamSinkStopped)
    CASE(MESourcePaused)
    CASE(MEStreamPaused)
    CASE(MEStreamSinkPaused)
    CASE(MESourceSeeked)
    CASE(MEStreamSeeked)
    CASE(MENewStream)
    CASE(MEUpdatedStream)
    CASE(MEMediaSample)
    CASE(MEEndOfPresentation)
    CASE(MEEndOfStream)
    CASE(MESourceRateChanged)
    CASE(MEStreamSinkRequestSample)
#undef CASE
    default: return "unknown";
    }
}

class media_source_reader_t {
    Microsoft::WRL::ComPtr<IMFMediaSource> source;
    std::set<Microsoft::WRL::ComPtr<IMFMediaStream>> streams;

public:
    media_source_reader_t(Microsoft::WRL::ComPtr<IMFMediaSource> source) : source(source) {
    }

    void read_all() {
        const auto start = std::chrono::steady_clock::now();
        Microsoft::WRL::ComPtr<IMFPresentationDescriptor> descr;
        MASSERTHR(this->source->CreatePresentationDescriptor(&descr));
        PROPVARIANT empty;
        PropVariantInit(&empty);
        MASSERTHR(this->source->Start(descr.Get(), nullptr, &empty));

        this->process_events(this->source, {MESourceStarted});
        for (auto &stream : this->streams) {
            this->process_events(stream, {MEStreamStarted});
        }

        while (!this->streams.empty()) {
            auto &stream = *std::begin(this->streams);
            MASSERTHR(stream->RequestSample(nullptr));
            if (this->process_events(stream, {MEMediaSample, MEEndOfStream}) == MEEndOfStream) {
                this->streams.erase(stream);
            }
        }

        this->process_events(this->source, {MEEndOfPresentation});
        const auto end = std::chrono::steady_clock::now();
        std::cout << "Decoding took " << std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count() << std::endl;
    }

private:
    Microsoft::WRL::ComPtr<IMFMediaEvent> get_event(Microsoft::WRL::ComPtr<IMFMediaEventGenerator> generator) {
        Microsoft::WRL::ComPtr<IMFMediaEvent> event;
        HRESULT hr = generator->GetEvent(0 /*MF_EVENT_FLAG_NO_WAIT*/, &event);
        if (hr == MF_E_NO_EVENTS_AVAILABLE) {
            return nullptr;
        }
        MASSERTHR(hr);
        return event;
    }

    double get_timestamp() {
        return std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::steady_clock::now().time_since_epoch()).count();
    }

    MediaEventType print_event(Microsoft::WRL::ComPtr<IMFMediaEvent> event) {
        HRESULT hr;
        MASSERTHR(event->GetStatus(&hr));
        MediaEventType type;
        MASSERTHR(event->GetType(&type));
        std::cout << /*std::fixed << get_timestamp() << */ ": event with result " << hr << " and type " << type << " (" << resolve_type(type) << ")" << std::endl;
        return type;
    }

    MediaEventType process_events(Microsoft::WRL::ComPtr<IMFMediaEventGenerator> generator, const std::set<MediaEventType> &until) {
        while (auto event = this->get_event(generator)) {
            const auto type = print_event(event);
            if (type == MENewStream) {
                PROPVARIANT variant;
                PropVariantInit(&variant);
                MASSERTHR(event->GetValue(&variant));
                MASSERT(variant.vt == VT_UNKNOWN);
                Microsoft::WRL::ComPtr<IMFMediaStream> stream;
                MASSERTHR(variant.punkVal->QueryInterface(IID_IMFMediaStream, &stream));
                this->streams.insert(std::move(stream));
                PropVariantClear(&variant);
            } else if (type == MEMediaSample) {
                PROPVARIANT variant;
                PropVariantInit(&variant);
                MASSERTHR(event->GetValue(&variant));
                MASSERT(variant.vt == VT_UNKNOWN);
                Microsoft::WRL::ComPtr<IMFSample> sample;
                MASSERTHR(variant.punkVal->QueryInterface(IID_IMFSample, &sample));
                LONGLONG time;
                MASSERTHR(sample->GetSampleTime(&time));
                LONGLONG duration;
                MASSERTHR(sample->GetSampleDuration(&duration));
                std::cout << "Sample has time " << double(time) / 10000000 << " and duration " << double(duration) / 10000000 << std::endl;
                PropVariantClear(&variant);
            }
            if (until.find(type) != std::end(until)) {
                return type;
            }
        }
        return 0;
    }
};

static void read_media_source(Microsoft::WRL::ComPtr<IMFMediaSource> media_source) {
    media_source_reader_t reader(media_source);
    reader.read_all();
}

static void probe_files(std::wstring filenames) {
    while (!filenames.empty()) {
        auto pos = filenames.find(' ');
        const auto filename = filenames.substr(0, pos);
        if (pos == std::wstring::npos) {
            filenames.clear();
        } else {
            filenames = filenames.substr(pos+1);
        }

        Microsoft::WRL::ComPtr<IMFByteStream> byte_stream;
        MASSERTHR(MFCreateFile(MF_ACCESSMODE_READ, MF_OPENMODE_FAIL_IF_NOT_EXIST, MF_FILEFLAGS_NONE,
                               filename.c_str(), &byte_stream));
        Microsoft::WRL::ComPtr<IMFSourceResolver> resolver;
        MASSERTHR(MFCreateSourceResolver(&resolver));
        MF_OBJECT_TYPE type;
        Microsoft::WRL::ComPtr<IUnknown> unk_media_source;
        MASSERTHR(resolver->CreateObjectFromByteStream(byte_stream.Get(), filename.c_str(), MF_RESOLUTION_MEDIASOURCE,
                                                       nullptr, &type, &unk_media_source));
        Microsoft::WRL::ComPtr<IMFMediaSource> media_source;
        MASSERTHR(unk_media_source.As<IMFMediaSource>(&media_source));

        Microsoft::WRL::ComPtr<IMFPresentationDescriptor> presentation_descr;
        MASSERTHR(media_source->CreatePresentationDescriptor(&presentation_descr));
        DWORD stream_count;
        MASSERTHR(presentation_descr->GetStreamDescriptorCount(&stream_count));
        for (DWORD i = 0; i < stream_count; i++) {
            Microsoft::WRL::ComPtr<IMFMediaType> media_type;
            Microsoft::WRL::ComPtr<IMFStreamDescriptor> stream_descr;
            BOOL selected;
            MASSERTHR(presentation_descr->GetStreamDescriptorByIndex(i, &selected, &stream_descr));
            Microsoft::WRL::ComPtr<IMFMediaTypeHandler> media_type_handler;
            MASSERTHR(stream_descr->GetMediaTypeHandler(&media_type_handler));
            DWORD media_type_count;
            MASSERTHR(media_type_handler->GetMediaTypeCount(&media_type_count));
            for (DWORD j = 0; j < media_type_count; j++) {
                Microsoft::WRL::ComPtr<IMFMediaType> media_type;
                MASSERTHR(media_type_handler->GetMediaTypeByIndex(j, &media_type));
                //dump_media_type(media_type.Get());
                UINT32 depth;
                if (SUCCEEDED(media_type->GetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, &depth))) {
                    std::wcout << filename << ": depth " << depth << std::endl;
                }
            }
        }

        Microsoft::WRL::ComPtr<IMFGetService> get_service;
        MASSERTHR(media_source.As<IMFGetService>(&get_service));
        Microsoft::WRL::ComPtr<IMFRateSupport> rate_support;
        MASSERTHR(get_service->GetService(MF_RATE_CONTROL_SERVICE, IID_IMFRateSupport, &rate_support));
        std::array<float, 4> rates;
        MASSERTHR(rate_support->GetFastestRate(MFRATE_FORWARD, false, &rates[0]));
        MASSERTHR(rate_support->GetFastestRate(MFRATE_FORWARD, true, &rates[1]));
        MASSERTHR(rate_support->GetFastestRate(MFRATE_REVERSE, false, &rates[2]));
        MASSERTHR(rate_support->GetFastestRate(MFRATE_REVERSE, true, &rates[3]));
        std::wcout << filename << ": fastest rates " << rates[0] << ' ' << rates[1] << ' ' << rates[2] << ' ' << rates[3] << std::endl;
        MASSERTHR(rate_support->GetSlowestRate(MFRATE_FORWARD, false, &rates[0]));
        MASSERTHR(rate_support->GetSlowestRate(MFRATE_FORWARD, true, &rates[1]));
        MASSERTHR(rate_support->GetSlowestRate(MFRATE_REVERSE, false, &rates[2]));
        MASSERTHR(rate_support->GetSlowestRate(MFRATE_REVERSE, true, &rates[3]));
        std::wcout << filename << ": slowest rates " << rates[0] << ' ' << rates[1] << ' ' << rates[2] << ' ' << rates[3] << std::endl;

        Microsoft::WRL::ComPtr<IMFRateControl> rate_control;
        MASSERTHR(get_service->GetService(MF_RATE_CONTROL_SERVICE, IID_IMFRateControl, &rate_control));
        MASSERTHR(rate_control->SetRate(false, 1.0));
        read_media_source(media_source);
    }
}

GIO_WWINMAIN(mf_probe_files) {
    (void)hInstance;
    (void)hPrevInstance;
    (void)nCmdShow;
    MASSERTHR(MFStartup(MF_VERSION, MFSTARTUP_FULL));
    probe_files(pCmdLine);
    MASSERTHR(MFShutdown());
    return 0;
}

void read_audio_stream(const std::wstring &filename, fire::optional<uint32_t> media_type_idx) {
    Microsoft::WRL::ComPtr<IMFByteStream> byte_stream;
    Microsoft::WRL::ComPtr<IMFSourceResolver> resolver;
    Microsoft::WRL::ComPtr<IUnknown> unk_media_source;
    Microsoft::WRL::ComPtr<IMFMediaSource> media_source;
    Microsoft::WRL::ComPtr<IMFSourceReader> reader;
    Microsoft::WRL::ComPtr<IMFMediaType> media_type;

    MASSERTHR(MFCreateFile(MF_ACCESSMODE_READ, MF_OPENMODE_FAIL_IF_NOT_EXIST, MF_FILEFLAGS_NONE, filename.c_str(), &byte_stream));
    MASSERTHR(MFCreateSourceResolver(&resolver));
    MF_OBJECT_TYPE type;
    MASSERTHR(resolver->CreateObjectFromByteStream(byte_stream.Get(), filename.c_str(), MF_RESOLUTION_MEDIASOURCE, nullptr, &type, &unk_media_source));
    MASSERTHR(unk_media_source.As<IMFMediaSource>(&media_source));
    MASSERTHR(MFCreateSourceReaderFromMediaSource(media_source.Get(), nullptr, &reader));
    MASSERTHR(reader->SetStreamSelection(MF_SOURCE_READER_ALL_STREAMS, FALSE));
    MASSERTHR(reader->SetStreamSelection(MF_SOURCE_READER_FIRST_AUDIO_STREAM, TRUE));

    std::vector<Microsoft::WRL::ComPtr<IMFMediaType>> media_types;
    for (int i = 0; ; i++) {
        HRESULT hr = reader->GetNativeMediaType(MF_SOURCE_READER_FIRST_AUDIO_STREAM, i, &media_type);
        if (hr == MF_E_NO_MORE_TYPES) {
            break;
        }
        MASSERTHR(hr);
        std::cout << "Media type " << i << std::endl;
        dump_media_type(media_type.Get());
        media_types.push_back(media_type);
    }

    MASSERTHR(reader->GetCurrentMediaType(MF_SOURCE_READER_FIRST_AUDIO_STREAM, &media_type));
    std::cout << "Current media type" << std::endl;
    dump_media_type(media_type.Get());

    if (media_type_idx.has_value()) {
        if (media_type_idx.value() >= media_types.size()) {
            std::cout << "Cannot select media type " << media_type_idx.value() << std::endl;
            return;
        }
        MASSERTHR(reader->SetCurrentMediaType(MF_SOURCE_READER_FIRST_AUDIO_STREAM, nullptr, media_types.at(media_type_idx.value()).Get()));

        MASSERTHR(reader->GetCurrentMediaType(MF_SOURCE_READER_FIRST_AUDIO_STREAM, &media_type));
        std::cout << "Media type after selection" << std::endl;
        dump_media_type(media_type.Get());
    }

    std::ofstream fout("out", std::ios::binary | std::ios::trunc | std::ios::out);
    while (true) {
        Microsoft::WRL::ComPtr<IMFSample> sample;
        DWORD stream_flags;
        MASSERTHR(reader->ReadSample(MF_SOURCE_READER_FIRST_AUDIO_STREAM, 0, nullptr, &stream_flags, nullptr, &sample));
        if (stream_flags & MF_SOURCE_READERF_ENDOFSTREAM) {
            break;
        }
        if (sample) {
            Microsoft::WRL::ComPtr<IMFMediaBuffer> buffer;
            MASSERTHR(sample->ConvertToContiguousBuffer(&buffer));
            BYTE *buf;
            DWORD len;
            MASSERTHR(buffer->Lock(&buf, nullptr, &len));
            fout.write(reinterpret_cast<char*>(buf), len);
        }
    }
}

int mf_read_audio_stream_main(std::string filename = fire::arg({0, "filename"}),
                      fire::optional<uint32_t> media_type_idx = fire::arg({"-i", "media type index"})) {
    std::wstring wfilename = gio::utf8_string_to_utf16_wstring(filename);
    MASSERTHR(MFStartup(MF_VERSION, MFSTARTUP_FULL));
    read_audio_stream(wfilename, media_type_idx);
    MASSERTHR(MFShutdown());
    return 0;
}

GIO_WWINMAIN_FIRE_STD_NO_SPACE_ASSIGNMENT(mf_read_audio_stream);

void read_video_stream(const std::wstring &filename, fire::optional<uint32_t> media_type_idx) {
    static const D3D_FEATURE_LEVEL levels[] = {D3D_FEATURE_LEVEL_11_0};
    Microsoft::WRL::ComPtr<IMFByteStream> byte_stream;
    Microsoft::WRL::ComPtr<IMFSourceResolver> resolver;
    Microsoft::WRL::ComPtr<IUnknown> unk_media_source;
    Microsoft::WRL::ComPtr<IMFMediaSource> media_source;
    Microsoft::WRL::ComPtr<IMFSourceReader> reader;
    Microsoft::WRL::ComPtr<IMFMediaType> media_type;
    Microsoft::WRL::ComPtr<IMFAttributes> attributes;
    Microsoft::WRL::ComPtr<IMFDXGIDeviceManager> device_manager;
    Microsoft::WRL::ComPtr<ID3D11Device> device;
    Microsoft::WRL::ComPtr<ID3D11DeviceContext> device_context;

    MASSERTHR(D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, 0, levels, sizeof(levels) / sizeof(*levels), D3D11_SDK_VERSION, &device, nullptr, &device_context));

    MASSERTHR(MFCreateFile(MF_ACCESSMODE_READ, MF_OPENMODE_FAIL_IF_NOT_EXIST, MF_FILEFLAGS_NONE, filename.c_str(), &byte_stream));
    MASSERTHR(MFCreateSourceResolver(&resolver));
    MF_OBJECT_TYPE type;
    MASSERTHR(resolver->CreateObjectFromByteStream(byte_stream.Get(), filename.c_str(), MF_RESOLUTION_MEDIASOURCE, nullptr, &type, &unk_media_source));
    MASSERTHR(unk_media_source.As<IMFMediaSource>(&media_source));
    UINT reset_token;
    MASSERTHR(MFCreateDXGIDeviceManager(&reset_token, &device_manager));
    MASSERTHR(device_manager->ResetDevice(device.Get(), reset_token));
    MASSERTHR(MFCreateAttributes(&attributes, 10));
    MASSERTHR(attributes->SetUnknown(MF_SOURCE_READER_D3D_MANAGER, device_manager.Get()));
    MASSERTHR(attributes->SetUINT32(MF_SOURCE_READER_ENABLE_ADVANCED_VIDEO_PROCESSING, TRUE));
    MASSERTHR(MFCreateSourceReaderFromMediaSource(media_source.Get(), attributes.Get(), &reader));
    MASSERTHR(reader->SetStreamSelection(MF_SOURCE_READER_ALL_STREAMS, FALSE));
    MASSERTHR(reader->SetStreamSelection(MF_SOURCE_READER_FIRST_VIDEO_STREAM, TRUE));

    std::vector<Microsoft::WRL::ComPtr<IMFMediaType>> media_types;
    for (int i = 0; ; i++) {
        HRESULT hr = reader->GetNativeMediaType(MF_SOURCE_READER_FIRST_VIDEO_STREAM, i, &media_type);
        if (hr == MF_E_NO_MORE_TYPES) {
            break;
        }
        MASSERTHR(hr);
        std::cout << "Media type " << i << std::endl;
        dump_media_type(media_type.Get());
        media_types.push_back(media_type);
    }

    MASSERTHR(reader->GetCurrentMediaType(MF_SOURCE_READER_FIRST_VIDEO_STREAM, &media_type));
    std::cout << "Current media type" << std::endl;
    dump_media_type(media_type.Get());

    if (media_type_idx.has_value()) {
        if (media_type_idx.value() >= media_types.size()) {
            std::cout << "Cannot select media type " << media_type_idx.value() << std::endl;
            return;
        }
        MASSERTHR(reader->SetCurrentMediaType(MF_SOURCE_READER_FIRST_VIDEO_STREAM, nullptr, media_types.at(media_type_idx.value()).Get()));

        MASSERTHR(reader->GetCurrentMediaType(MF_SOURCE_READER_FIRST_VIDEO_STREAM, &media_type));
        std::cout << "Media type after selection" << std::endl;
        dump_media_type(media_type.Get());
    } else {
        MASSERTHR(MFCreateMediaType(&media_type));
        MASSERTHR(media_type->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video));
        MASSERTHR(media_type->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_ARGB32));
        MASSERTHR(reader->SetCurrentMediaType(MF_SOURCE_READER_FIRST_VIDEO_STREAM, nullptr, media_type.Get()));

        MASSERTHR(reader->GetCurrentMediaType(MF_SOURCE_READER_FIRST_VIDEO_STREAM, &media_type));
        std::cout << "Media type after selection" << std::endl;
        dump_media_type(media_type.Get());
    }

    Microsoft::WRL::ComPtr<IMFTransform> transform;
    MASSERTHR(reader->GetServiceForStream(MF_SOURCE_READER_FIRST_VIDEO_STREAM, GUID_NULL, IID_IMFTransform, &transform));
    Microsoft::WRL::ComPtr<IMFAttributes> stream_attributes;
    MASSERTHR(transform->GetOutputStreamAttributes(0, &stream_attributes));
    MASSERTHR(stream_attributes->SetUINT32(MF_SA_D3D11_USAGE, D3D11_USAGE_DEFAULT));
    MASSERTHR(stream_attributes->SetUINT32(MF_SA_D3D11_SHARED_WITHOUT_MUTEX, TRUE));

    while (true) {
        Microsoft::WRL::ComPtr<IMFSample> sample;
        DWORD stream_flags;
        MASSERTHR(reader->ReadSample(MF_SOURCE_READER_FIRST_VIDEO_STREAM, 0, nullptr, &stream_flags, nullptr, &sample));
        if (stream_flags & MF_SOURCE_READERF_ENDOFSTREAM) {
            break;
        }
        if (sample) {
            LONGLONG time, duration;
            MASSERTHR(sample->GetSampleTime(&time));
            MASSERTHR(sample->GetSampleDuration(&duration));
            Microsoft::WRL::ComPtr<IMFMediaBuffer> buffer;
            MASSERTHR(sample->ConvertToContiguousBuffer(&buffer));
            DWORD length;
            MASSERTHR(buffer->GetCurrentLength(&length));
            std::cout << "Got sample of length " << length << " and duration " << duration << " at time " << time << std::endl;
            /*BYTE *buf;
            DWORD len;
            MASSERTHR(buffer->Lock(&buf, nullptr, &len));*/
            Microsoft::WRL::ComPtr<IMFDXGIBuffer> dxgi_buffer;
            MASSERTHR(buffer.As(&dxgi_buffer));
            Microsoft::WRL::ComPtr<ID3D11Texture2D> texture;
            Microsoft::WRL::ComPtr<IDXGIResource> resource;
            //MASSERTHR(dxgi_buffer->GetResource(IID_ID3D11Texture2D, &texture));
            //MASSERTHR(dxgi_buffer.As(&resource));
            MASSERTHR(dxgi_buffer->GetResource(IID_IDXGIResource, &resource));
            HANDLE handle;
            MASSERTHR(resource->GetSharedHandle(&handle));
            std::cout << "Got handle " << handle << std::endl;
            MASSERT(handle != nullptr);
        }
    }
}

int mf_read_video_stream_main(std::string filename = fire::arg({0, "filename"}),
                      fire::optional<uint32_t> media_type_idx = fire::arg({"-i", "media type index"})) {
    std::wstring wfilename = gio::utf8_string_to_utf16_wstring(filename);
    MASSERTHR(MFStartup(MF_VERSION, MFSTARTUP_FULL));
    read_video_stream(wfilename, media_type_idx);
    MASSERTHR(MFShutdown());
    return 0;
}

GIO_WWINMAIN_FIRE_STD_NO_SPACE_ASSIGNMENT(mf_read_video_stream);

class sample_grabber_callback_t final : public IMFSampleGrabberSinkCallback {
public:
    static HRESULT CreateInstance(sample_grabber_callback_t **ptr) {
        if (!ptr) {
            return E_POINTER;
        }
        auto callback = new(std::nothrow) sample_grabber_callback_t();
        if (!callback) {
            return E_OUTOFMEMORY;
        }
        *ptr = callback;
        std::cout << "Create " << *ptr << std::endl;
        return S_OK;
    }

    STDMETHODIMP QueryInterface(REFIID riid, void **ptr) {
        static const QITAB qit[] = {
            QITABENT(sample_grabber_callback_t, IMFSampleGrabberSinkCallback),
            {0, 0},
        };
        return QISearch(this, qit, riid, ptr);
    }

    STDMETHODIMP_(ULONG) AddRef() {
        ULONG count = InterlockedIncrement(&this->refcount);
        std::cout << "AddRef to " << count << " on " << this << std::endl;
        return count;
    }

    STDMETHODIMP_(ULONG) Release() {
        ULONG count = InterlockedDecrement(&this->refcount);
        std::cout << "Release to " << count << " on " << this << std::endl;
        if (count == 0) {
            std::cout << "Destroy " << this << std::endl;
            delete this;
        }
        return count;
    }

    STDMETHODIMP OnClockPause(MFTIME system_time) {
        std::cout << "OnClockPause " << this << " " << system_time << std::endl;
        this->state = "paused";
        return S_OK;
    }

    STDMETHODIMP OnClockRestart(MFTIME system_time) {
        std::cout << "OnClockRestart " << this << " " << system_time << std::endl;
        this->state = "restarted";
        return S_OK;
    }

    STDMETHODIMP OnClockSetRate(MFTIME system_time, float rate) {
        std::cout << "OnClockSetRate " << this << " " << system_time << " " << rate << std::endl;
        return S_OK;
    }

    STDMETHODIMP OnClockStart(MFTIME system_time, LONGLONG offset) {
        std::cout << "OnClockStart " << this << " " << system_time << " " << offset << std::endl;
        this->state = "started";
        return S_OK;
    }

    STDMETHODIMP OnClockStop(MFTIME system_time) {
        std::cout << "OnClockStop " << this << " " << system_time << std::endl;
        this->state = "stopped";
        return S_OK;
    }

    STDMETHODIMP OnProcessSample(REFGUID major, DWORD flags, LONGLONG time, LONGLONG duration, const BYTE *buf, DWORD size) {
        std::cout << "OnProcessSample " << this << " " << time << " " << duration << " " << size << std::endl;
        return S_OK;
    }

    STDMETHODIMP OnSetPresentationClock(IMFPresentationClock *clock) {
        std::cout << "OnSetPresentationClock " << this << " " << clock << std::endl;
        return S_OK;
    }

    STDMETHODIMP OnShutdown() {
        std::cout << "OnShutdown " << this << std::endl;
        return S_OK;
    }

    void check_state(const std::string &expect) {
        if (this->state != expect) {
            std::cout << "Bad state '" << this->state << "' instead of '" << expect << "'" << std::endl;
            exit(1);
        }
        this->state = "";
    }

protected:
    sample_grabber_callback_t() : refcount(1) {
    }

    virtual ~sample_grabber_callback_t() {
    }

    ULONG refcount;
    Microsoft::WRL::ComPtr<IMFSourceReader> reader;
    std::set<size_t> finished;
    std::mutex mutex;
    std::string state;
};

static void expect_event(IMFMediaEventGenerator *gen, MediaEventType met) {
    Microsoft::WRL::ComPtr<IMFMediaEvent> event;
    MASSERTHR(gen->GetEvent(0, &event));
    MediaEventType got_met;
    MASSERTHR(event->GetType(&got_met));
    if (met != got_met) {
        std::cout << "Got " << resolve_type(got_met) << " instead of " << resolve_type(met) << std::endl;
    }
}

static void expect_no_event(IMFMediaEventGenerator *gen) {
    Microsoft::WRL::ComPtr<IMFMediaEvent> event;
    MASSERT(gen->GetEvent(MF_EVENT_FLAG_NO_WAIT, &event) == MF_E_NO_EVENTS_AVAILABLE);
}

static void test_sample_grabber() {
    Microsoft::WRL::ComPtr<IMFMediaType> media_type;
    MASSERTHR(MFCreateMediaType(&media_type));
    MASSERTHR(media_type->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio));
    MASSERTHR(media_type->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_PCM));
    Microsoft::WRL::ComPtr<sample_grabber_callback_t> callback;
    MASSERTHR(sample_grabber_callback_t::CreateInstance(&callback));
    Microsoft::WRL::ComPtr<IMFActivate> activate;
    MASSERTHR(MFCreateSampleGrabberSinkActivate(media_type.Get(), callback.Get(), &activate));
    MASSERTHR(activate->SetUINT32(MF_SAMPLEGRABBERSINK_IGNORE_CLOCK, TRUE));
    Microsoft::WRL::ComPtr<IMFMediaSink> sink;
    MASSERTHR(activate->ActivateObject(IID_IMFMediaSink, &sink));
    DWORD count;
    MASSERTHR(sink->GetStreamSinkCount(&count));
    std::cout << "Count: " << count << std::endl;
    Microsoft::WRL::ComPtr<IMFStreamSink> stream;
    MASSERTHR(sink->GetStreamSinkByIndex(0, &stream));
    Microsoft::WRL::ComPtr<IMFClockStateSink> clock_sink;
    MASSERTHR(sink.As<IMFClockStateSink>(&clock_sink));
    callback->check_state("");
    MASSERTHR(clock_sink->OnClockStop(MFGetSystemTime()));
    expect_event(stream.Get(), MEStreamSinkStopped);
    callback->check_state("stopped");
    MASSERTHR(clock_sink->OnClockStop(MFGetSystemTime()));
    expect_event(stream.Get(), MEStreamSinkStopped);
    callback->check_state("stopped");
    MASSERT(clock_sink->OnClockPause(MFGetSystemTime()) == MF_E_INVALID_STATE_TRANSITION);
    callback->check_state("");
    MASSERTHR(clock_sink->OnClockStart(MFGetSystemTime(), 0));
    expect_event(stream.Get(), MEStreamSinkRequestSample);
    expect_event(stream.Get(), MEStreamSinkRequestSample);
    expect_event(stream.Get(), MEStreamSinkRequestSample);
    expect_event(stream.Get(), MEStreamSinkRequestSample);
    expect_event(stream.Get(), MEStreamSinkStarted);
    callback->check_state("started");
    //callback->check_state("");
    MASSERTHR(clock_sink->OnClockStart(MFGetSystemTime(), 0));
    expect_event(stream.Get(), MEStreamSinkStarted);
    callback->check_state("started");
    //callback->check_state("");
    MASSERTHR(clock_sink->OnClockStart(MFGetSystemTime(), 0));
    expect_event(stream.Get(), MEStreamSinkStarted);
    callback->check_state("started");
    //callback->check_state("");
    MASSERTHR(clock_sink->OnClockRestart(MFGetSystemTime()));
    expect_event(stream.Get(), MEStreamSinkStarted);
    callback->check_state("started");
    //callback->check_state("");
    MASSERTHR(clock_sink->OnClockRestart(MFGetSystemTime()));
    expect_event(stream.Get(), MEStreamSinkStarted);
    callback->check_state("started");
    //callback->check_state("");
    MASSERTHR(clock_sink->OnClockStart(MFGetSystemTime(), 0));
    expect_event(stream.Get(), MEStreamSinkStarted);
    callback->check_state("started");
    //callback->check_state("");
    MASSERTHR(clock_sink->OnClockPause(MFGetSystemTime()));
    expect_event(stream.Get(), MEStreamSinkPaused);
    callback->check_state("paused");
    MASSERTHR(clock_sink->OnClockPause(MFGetSystemTime()));
    callback->check_state("");
    MASSERTHR(clock_sink->OnClockPause(MFGetSystemTime()));
    callback->check_state("");
    MASSERTHR(clock_sink->OnClockStart(MFGetSystemTime(), 0));
    expect_event(stream.Get(), MEStreamSinkStarted);
    callback->check_state("started");
    //callback->check_state("");
    MASSERTHR(clock_sink->OnClockStart(MFGetSystemTime(), 0));
    expect_event(stream.Get(), MEStreamSinkStarted);
    callback->check_state("started");
    //callback->check_state("");
    MASSERTHR(clock_sink->OnClockStop(MFGetSystemTime()));
    expect_event(stream.Get(), MEStreamSinkStopped);
    callback->check_state("stopped");
    MASSERTHR(clock_sink->OnClockStop(MFGetSystemTime()));
    expect_event(stream.Get(), MEStreamSinkStopped);
    callback->check_state("stopped");
    MASSERTHR(clock_sink->OnClockStop(MFGetSystemTime()));
    expect_event(stream.Get(), MEStreamSinkStopped);
    callback->check_state("stopped");
    expect_no_event(stream.Get());
    callback->check_state("");
    MASSERTHR(sink->Shutdown());
}

int mf_test_sample_grabber_main() {
    MASSERTHR(MFStartup(MF_VERSION, MFSTARTUP_FULL));
    test_sample_grabber();
    MASSERTHR(MFShutdown());
    return 0;
}

GIO_WWINMAIN_FIRE_STD_NO_SPACE_ASSIGNMENT(mf_test_sample_grabber);

class source_t final : public IMFMediaSource, public IMFGetService, public IMFRateSupport {
public:
    static HRESULT CreateInstance(std::vector<float> rates, IMFMediaSource **ptr) {
        if (!ptr) {
            return E_POINTER;
        }
        auto callback = new(std::nothrow) source_t(std::move(rates));
        if (!callback) {
            return E_OUTOFMEMORY;
        }
        *ptr = callback;
        std::cout << "Create " << *ptr << std::endl;
        return S_OK;
    }

    STDMETHODIMP QueryInterface(REFIID riid, void **ptr) {
        static const QITAB qit[] = {
            QITABENT(source_t, IMFMediaSource),
            QITABENT(source_t, IMFGetService),
            QITABENT(source_t, IMFRateSupport),
            {0, 0},
        };
        HRESULT ret = QISearch(this, qit, riid, ptr);
        WCHAR buf[1024];
        MASSERTHR(StringFromGUID2(riid, buf, 1024));
        std::wcout << "Querying " << buf << " with result " << std::hex << ret << std::endl;
        return ret;
    }

    STDMETHODIMP_(ULONG) AddRef() {
        ULONG count = InterlockedIncrement(&this->refcount);
        std::cout << "AddRef to " << count << " on " << this << std::endl;
        return count;
    }

    STDMETHODIMP_(ULONG) Release() {
        ULONG count = InterlockedDecrement(&this->refcount);
        std::cout << "Release to " << count << " on " << this << std::endl;
        if (count == 0) {
            std::cout << "Destroy " << this << std::endl;
            delete this;
        }
        return count;
    }

    STDMETHODIMP BeginGetEvent(IMFAsyncCallback *callback, IUnknown *state) {
        return this->queue->BeginGetEvent(callback, state);
    }

    STDMETHODIMP EndGetEvent(IMFAsyncResult *result, IMFMediaEvent **event) {
        return this->queue->EndGetEvent(result, event);
    }

    STDMETHODIMP GetEvent(DWORD flags, IMFMediaEvent **event) {
        return this->queue->GetEvent(flags, event);
    }

    STDMETHODIMP QueueEvent(MediaEventType met, REFGUID guid, HRESULT status, const PROPVARIANT *pv) {
        return this->queue->QueueEventParamVar(met, guid, status, pv);
    }

    STDMETHODIMP CreatePresentationDescriptor(IMFPresentationDescriptor **pd) {
        std::cout << "CreatePresentationDescriptor" << std::endl;
        Microsoft::WRL::ComPtr<IMFMediaType> media_type;
        MASSERTHR(MFCreateMediaType(&media_type));
        MASSERTHR(media_type->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio));
        MASSERTHR(media_type->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_PCM));
        Microsoft::WRL::ComPtr<IMFStreamDescriptor> sd;
        MASSERTHR(MFCreateStreamDescriptor(0, 1, media_type.GetAddressOf(), &sd));
        MASSERTHR(MFCreatePresentationDescriptor(1, sd.GetAddressOf(), pd));
        return S_OK;
    }

    STDMETHODIMP GetCharacteristics(DWORD *chars) {
        std::cout << "GetCharacteristics" << std::endl;
        chars = 0;
        return S_OK;
    }

    STDMETHODIMP Pause() {
        std::cout << "Pause" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP Shutdown() {
        std::cout << "Shutdown" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP Start(IMFPresentationDescriptor *pd, const GUID *guid, const PROPVARIANT *time) {
        std::cout << "Start" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP Stop() {
        std::cout << "Stop" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP GetService(REFGUID guid, REFIID iid, LPVOID *object) {
        std::cout << "GetService" << std::endl;
        return this->QueryInterface(iid, object);
    }

    STDMETHODIMP GetFastestRate(MFRATE_DIRECTION dir, BOOL thin, float *rate) {
        std::cout << "GetFastestRate" << std::endl;
        int idx = (!!thin << 1) + (dir == MFRATE_REVERSE);
        *rate = this->rates.at(idx);
        return S_OK;
    }

    STDMETHODIMP GetSlowestRate(MFRATE_DIRECTION dir, BOOL thin, float *rate) {
        std::cout << "GetSlowestRate" << std::endl;
        int idx = (!!thin << 1) + (dir == MFRATE_REVERSE) + 4;
        *rate = this->rates.at(idx);
        return S_OK;
    }

    STDMETHODIMP IsRateSupported(BOOL thin, float rate, float *near) {
        std::cout << "IsRateSupported" << std::endl;
        return E_NOTIMPL;
    }

protected:
    source_t(std::vector<float> &&rates) : refcount(1), rates(std::move(rates)) {
        MASSERTHR(MFCreateEventQueue(&this->queue));
    }

    virtual ~source_t() {
    }

    ULONG refcount;
    Microsoft::WRL::ComPtr<IMFMediaEventQueue> queue;
    std::vector<float> rates;
};

class sink_t : public IMFMediaSink, public IMFStreamSink, public IMFGetService, public IMFRateSupport {
public:
    static HRESULT CreateInstance(std::vector<float> rates, IMFMediaSink **ptr) {
        if (!ptr) {
            return E_POINTER;
        }
        auto callback = new(std::nothrow) sink_t(std::move(rates));
        if (!callback) {
            return E_OUTOFMEMORY;
        }
        *ptr = callback;
        std::cout << "Create " << *ptr << std::endl;
        return S_OK;
    }

    STDMETHODIMP QueryInterface(REFIID riid, void **ptr) {
        static const QITAB qit[] = {
            QITABENT(sink_t, IMFMediaSink),
            QITABENT(sink_t, IMFStreamSink),
            QITABENT(sink_t, IMFGetService),
            QITABENT(sink_t, IMFRateSupport),
            {0, 0},
        };
        HRESULT ret = QISearch(this, qit, riid, ptr);
        WCHAR buf[1024];
        MASSERTHR(StringFromGUID2(riid, buf, 1024));
        std::wcout << "Querying " << buf << " with result " << std::hex << ret << std::endl;
        return ret;
    }

    STDMETHODIMP_(ULONG) AddRef() {
        ULONG count = InterlockedIncrement(&this->refcount);
        std::cout << "AddRef to " << count << " on " << this << std::endl;
        return count;
    }

    STDMETHODIMP_(ULONG) Release() {
        ULONG count = InterlockedDecrement(&this->refcount);
        std::cout << "Release to " << count << " on " << this << std::endl;
        if (count == 0) {
            std::cout << "Destroy " << this << std::endl;
            delete this;
        }
        return count;
    }

    STDMETHODIMP BeginGetEvent(IMFAsyncCallback *callback, IUnknown *state) {
        return this->queue->BeginGetEvent(callback, state);
    }

    STDMETHODIMP EndGetEvent(IMFAsyncResult *result, IMFMediaEvent **event) {
        return this->queue->EndGetEvent(result, event);
    }

    STDMETHODIMP GetEvent(DWORD flags, IMFMediaEvent **event) {
        return this->queue->GetEvent(flags, event);
    }

    STDMETHODIMP QueueEvent(MediaEventType met, REFGUID guid, HRESULT status, const PROPVARIANT *pv) {
        return this->queue->QueueEventParamVar(met, guid, status, pv);
    }

    STDMETHODIMP AddStreamSink(DWORD id, IMFMediaType *media_type, IMFStreamSink **stream) {
        std::cout << "AddStreamSink" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP GetCharacteristics(DWORD *chars) {
        std::cout << "GetCharacteristics" << std::endl;
        *chars = MEDIASINK_FIXED_STREAMS;
        return S_OK;
    }

    STDMETHODIMP GetPresentationClock(IMFPresentationClock **clock) {
        std::cout << "GetPresentationClock" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP GetStreamSinkById(DWORD id, IMFStreamSink **stream) {
        std::cout << "GetStreamSinkById" << std::endl;
        MASSERTHR(this->QueryInterface(IID_IMFStreamSink, reinterpret_cast<void**>(stream)));
        return S_OK;
    }

    STDMETHODIMP GetStreamSinkByIndex(DWORD id, IMFStreamSink **stream) {
        std::cout << "GetStreamSinkByIndex" << std::endl;
        MASSERTHR(this->QueryInterface(IID_IMFStreamSink, reinterpret_cast<void**>(stream)));
        return S_OK;
    }

    STDMETHODIMP GetStreamSinkCount(DWORD *count) {
        std::cout << "GetStreamSinkCount" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP RemoveStreamSink(DWORD id) {
        std::cout << "RemoveStreamSink" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP SetPresentationClock(IMFPresentationClock *clock) {
        std::cout << "SetPresentationClock" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP Shutdown() {
        std::cout << "Shutdown" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP Flush() {
        std::cout << "Flush" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP GetIdentifier(DWORD *id) {
        std::cout << "GetIdentifier" << std::endl;
        *id = 0;
        return S_OK;
    }

    STDMETHODIMP GetMediaSink(IMFMediaSink **sink) {
        std::cout << "GetMediaSink" << std::endl;
        MASSERTHR(this->QueryInterface(IID_IMFMediaSink, reinterpret_cast<void**>(sink)));
        return S_OK;
    }

    STDMETHODIMP GetMediaTypeHandler(IMFMediaTypeHandler **handler) {
        std::cout << "GetMediaTypeHandler" << std::endl;
        MASSERTHR(MFCreateSimpleTypeHandler(handler));
        Microsoft::WRL::ComPtr<IMFMediaType> media_type;
        MASSERTHR(MFCreateMediaType(&media_type));
        MASSERTHR(media_type->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio));
        MASSERTHR(media_type->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_PCM));
        MASSERTHR((*handler)->SetCurrentMediaType(media_type.Get()));
        return S_OK;
    }

    STDMETHODIMP PlaceMarker(MFSTREAMSINK_MARKER_TYPE type, const PROPVARIANT *value, const PROPVARIANT *ctx) {
        std::cout << "PlaceMarker" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP ProcessSample(IMFSample *sample) {
        std::cout << "ProcessSample" << std::endl;
        return E_NOTIMPL;
    }

    STDMETHODIMP GetService(REFGUID guid, REFIID iid, LPVOID *object) {
        std::cout << "GetService" << std::endl;
        return this->QueryInterface(iid, object);
    }

    STDMETHODIMP GetFastestRate(MFRATE_DIRECTION dir, BOOL thin, float *rate) {
        std::cout << "GetFastestRate" << std::endl;
        int idx = (!!thin << 1) + (dir == MFRATE_REVERSE);
        *rate = this->rates.at(idx);
        return S_OK;
    }

    STDMETHODIMP GetSlowestRate(MFRATE_DIRECTION dir, BOOL thin, float *rate) {
        std::cout << "GetSlowestRate" << std::endl;
        int idx = (!!thin << 1) + (dir == MFRATE_REVERSE) + 4;
        *rate = this->rates.at(idx);
        return S_OK;
    }

    STDMETHODIMP IsRateSupported(BOOL thin, float rate, float *near) {
        std::cout << "IsRateSupported" << std::endl;
        return E_NOTIMPL;
    }

protected:
    sink_t(std::vector<float> &&rates) : refcount(1), rates(std::move(rates)) {
        MASSERTHR(MFCreateEventQueue(&this->queue));
    }

    virtual ~sink_t() {
    }

    ULONG refcount;
    Microsoft::WRL::ComPtr<IMFMediaEventQueue> queue;
    std::vector<float> rates;
};

std::vector<float> test_session_rate(std::vector<float> source_rates, std::vector<float> sink_rates) {
    Microsoft::WRL::ComPtr<IMFMediaSource> source;
    /*Microsoft::WRL::ComPtr<IMFSourceResolver> resolver;
    MASSERTHR(MFCreateSourceResolver(&resolver));
    MF_OBJECT_TYPE type;
    MASSERTHR(resolver->CreateObjectFromURL(wfilename.c_str(), MF_RESOLUTION_MEDIASOURCE | MF_RESOLUTION_READ, nullptr, &type, &source));
    MASSERT(type == MF_OBJECT_MEDIASOURCE);*/
    //std::vector<float> source_rates = {1e2f, -1e2f, 1e4f, -1e4f, 0.0f, 0.0f, 0.0f, 0.0f};
    MASSERTHR(source_t::CreateInstance(source_rates, &source));
    Microsoft::WRL::ComPtr<IMFPresentationDescriptor> pd;
    MASSERTHR(source->CreatePresentationDescriptor(&pd));
    MASSERTHR(pd->SelectStream(0));
    Microsoft::WRL::ComPtr<IMFStreamDescriptor> sd;
    BOOL selected;
    MASSERTHR(pd->GetStreamDescriptorByIndex(0, &selected, &sd));

    /*Microsoft::WRL::ComPtr<IMFMediaTypeHandler> handler;
    MASSERTHR(sd->GetMediaTypeHandler(&handler));
    Microsoft::WRL::ComPtr<IMFMediaType> media_type;
    MASSERTHR(handler->GetMediaTypeByIndex(0, &media_type));
    //dump_media_type(media_type.Get());
    //MASSERTHR(handler->SetCurrentMediaType(media_type.Get()));
    //MASSERTHR(MFCreateMediaType(&media_type));
    //MASSERTHR(media_type->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio));
    //MASSERTHR(media_type->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_PCM));
    Microsoft::WRL::ComPtr<IMFSampleGrabberSinkCallback> callback;
    MASSERTHR(sample_grabber_callback_t::CreateInstance(&callback));
    Microsoft::WRL::ComPtr<IMFActivate> activate;
    MASSERTHR(MFCreateSampleGrabberSinkActivate(media_type.Get(), callback.Get(), &activate));*/
    //std::vector<float> sink_rates = {1e2f, -1e2f, 1e4f, -1e4f, 0.0f, 0.0f, 0.0f, 0.0f};
    Microsoft::WRL::ComPtr<IMFMediaSink> sink;
    MASSERTHR(sink_t::CreateInstance(sink_rates, &sink));

    Microsoft::WRL::ComPtr<IMFMediaSession> session;
    MASSERTHR(MFCreateMediaSession(nullptr, &session));
    Microsoft::WRL::ComPtr<IMFTopology> topology;
    MASSERTHR(MFCreateTopology(&topology));
    Microsoft::WRL::ComPtr<IMFTopologyNode> node;
    MASSERTHR(MFCreateTopologyNode(MF_TOPOLOGY_SOURCESTREAM_NODE, &node));
    MASSERTHR(node->SetUnknown(MF_TOPONODE_SOURCE, source.Get()));
    MASSERTHR(node->SetUnknown(MF_TOPONODE_PRESENTATION_DESCRIPTOR, pd.Get()));
    MASSERTHR(node->SetUnknown(MF_TOPONODE_STREAM_DESCRIPTOR, sd.Get()));
    MASSERTHR(topology->AddNode(node.Get()));
    Microsoft::WRL::ComPtr<IMFTopologyNode> node2;
    MASSERTHR(MFCreateTopologyNode(MF_TOPOLOGY_OUTPUT_NODE, &node2));
    MASSERTHR(node2->SetObject(sink.Get()));
    MASSERTHR(topology->AddNode(node2.Get()));
    MASSERTHR(node->ConnectOutput(0, node2.Get(), 0));
    MASSERTHR(session->SetTopology(MFSESSION_SETTOPOLOGY_IMMEDIATE, topology.Get()));
    Sleep(10);
    //MASSERTHR(session->GetFullTopology(MFSESSION_GETFULLTOPOLOGY_CURRENT, 0, &topology));
    Microsoft::WRL::ComPtr<IMFGetService> get_service;
    MASSERTHR(session.As<IMFGetService>(&get_service));
    Microsoft::WRL::ComPtr<IMFRateSupport> rate_support;
    MASSERTHR(get_service->GetService(MF_RATE_CONTROL_SERVICE, IID_IMFRateSupport, &rate_support));
    std::vector<float> ret;
    ret.resize(8);
    for (int i = 0; i < 4; i++) {
        MASSERTHR(rate_support->GetFastestRate(i & 1 ? MFRATE_REVERSE : MFRATE_FORWARD, !!(i & 2), &ret[i]));
        MASSERTHR(rate_support->GetSlowestRate(i & 1 ? MFRATE_REVERSE : MFRATE_FORWARD, !!(i & 2), &ret[i+4]));
    }
    MASSERTHR(session->Shutdown());
    return ret;
}

static const std::vector<std::tuple<std::vector<float>, std::vector<float>, std::vector<float>>> test_data = {
    {{1e2, -1e2, 1e4, -1e4, 0, 0, 0, 0}, {1e2, -1e2, 1e4, -1e4, 0, 0, 0, 0}, {1e2, -1e2, 1e4, -1e4, 0, 0, 0, 0}},
    {{1e2, 1e2, -1e4, 1e4, 0, 0, 0, 0}, {1e2, 1e2, 1e4, -1e4, 0, 0, 0, 0}, {1e2, -1e2, 1e4, -1e4, 0, 0, 0, 0}},
    {{1e2, -1e2, 1e4, -1e4, 0, 0, 0, 0}, {1e1, -1e1, 1e3, -1e2, 0, 0, 0, 0}, {1e1, -1e1, 1e3, -1e2, 0, 0, 0, 0}},
    {{1e2, 1e2, -1e4, 1e4, 1e-2, -1e-2, 1e-2, -1e-2}, {1e2, 1e2, 1e4, -1e4, 0, 0, 0, 0}, {1e2, -1e2, 1e4, -1e4, 1e-2, -1e-2, 1e-2, -1e-2}},
    {{1e2, 1e2, -1e4, 1e4, 1e-2, -1e-2, 1e-2, -1e-2}, {1e2, 1e2, 1e4, -1e4, 1e-4, -1e-4, 1e-5, -1e-5}, {1e2, -1e2, 1e4, -1e4, 1e-2, -1e-2, 1e-2, -1e-2}},
    {{1e2, 1e2, -1e4, 1e4, 1e-2, 1e-2, -1e-2, -1e-2}, {1e2, 1e2, 1e4, -1e4, -1e-4, -1e-4, -1e-5, -1e-5}, {1e2, -1e2, 1e4, -1e4, 1e-2, -1e-2, 1e-2, -1e-2}},
};

static std::ostream &operator<<(std::ostream &os, const std::vector<float> &x) {
    os << "{";
    for (const auto &y : x) {
        os << y << ", ";
    }
    os << "}";
    return os;
}

int mf_test_session_rate_main(std::string filename = fire::arg({0, "filename"})) {
    std::wstring wfilename = gio::utf8_string_to_utf16_wstring(filename);
    MASSERTHR(MFStartup(MF_VERSION, MFSTARTUP_FULL));
    for (const auto &datum : test_data) {
        const auto res = test_session_rate(std::get<0>(datum), std::get<1>(datum));
        if (res != std::get<2>(datum)) {
            std::cout << "Wrong result " << res << " instead of " << std::get<2>(datum) << " between " << std::get<0>(datum) << " and " << std::get<1>(datum) << std::endl;
        }
    }
    MASSERTHR(MFShutdown());
    return 0;
}

GIO_WWINMAIN_FIRE_STD_NO_SPACE_ASSIGNMENT(mf_test_session_rate);

static std::ostream &operator<<(std::ostream &os, const std::vector<std::pair<int, int>> &x) {
    os << "{";
    for (const auto &y : x) {
        os << "(" << y.first << "," << y.second << "), ";
    }
    os << "}";
    return os;
}

static const int RUNS = 10;

void test_event_queue_race() {
    Microsoft::WRL::ComPtr<IMFMediaEventQueue> q1, q2;
    MFCreateEventQueue(&q1);
    MFCreateEventQueue(&q2);
    std::mutex m;
    std::condition_variable cv;
    std::vector<std::pair<int, int>> v;
    int listeners = 0;
    std::thread t1([&]() {
        for (int i = 0; i < RUNS; i++) {
            Microsoft::WRL::ComPtr<IMFMediaEvent> e;
            q1->GetEvent(0, &e);
            std::unique_lock l(m);
            v.push_back(std::make_pair(0, i));
            listeners++;
            cv.notify_all();
            cv.wait(l, [&]() { return listeners == 0; });
        }
    });
    std::thread t2([&]() {
        for (int i = 0; i < RUNS; i++) {
            Microsoft::WRL::ComPtr<IMFMediaEvent> e;
            q2->GetEvent(0, &e);
            std::unique_lock l(m);
            v.push_back(std::make_pair(1, i));
            listeners++;
            cv.notify_all();
            cv.wait(l, [&]() { return listeners == 0; });
        }
    });
    for (int i = 0; i < RUNS; i++) {
        std::cout << "Iteration " << i << std::endl;
        Microsoft::WRL::ComPtr<IMFMediaEvent> e1, e2;
        PROPVARIANT var;
        var.vt = VT_EMPTY;
        MFCreateMediaEvent(MESourceStarted, GUID_NULL, 0, &var, &e1);
        MFCreateMediaEvent(MESourceStarted, GUID_NULL, 0, &var, &e2);
        q1->QueueEvent(e1.Get());
        q2->QueueEvent(e2.Get());
        std::unique_lock l(m);
        cv.wait(l, [&]() { return listeners == 2; });
        listeners = 0;
        cv.notify_all();
    }
    t1.join();
    t2.join();
    std::cout << v << std::endl;
}

int mf_test_event_queue_race_main() {
    MASSERTHR(MFStartup(MF_VERSION, MFSTARTUP_FULL));
    test_event_queue_race();
    MASSERTHR(MFShutdown());
    return 0;
}

GIO_WWINMAIN_FIRE_STD_NO_SPACE_ASSIGNMENT(mf_test_event_queue_race);

class MFPlayWindow : public WindowBase {
public:
    MFPlayWindow(const wchar_t *class_name, const wchar_t *name, HINSTANCE instance) : WindowBase(class_name, name, instance) {
    }

    LRESULT handle(UINT uMsg, WPARAM wParam, LPARAM lParam) override {
        return this->default_handle(uMsg, wParam, lParam);
    }
};

static void mfplay(const std::wstring &filename, HINSTANCE hInstance, int nCmdShow) {
    WindowClass wclass(hInstance, L"Window Class");
    auto win = wclass.create<MFPlayWindow>(L"MFPlay test");
    win->show(nCmdShow);
    Microsoft::WRL::ComPtr<IMFPMediaPlayer> player;
    MASSERTHR(MFPCreateMediaPlayer(nullptr, FALSE, MFP_OPTION_NONE, nullptr, win->get_hwnd(), &player));
    Microsoft::WRL::ComPtr<IMFPMediaItem> item;
    MASSERTHR(player->CreateMediaItemFromURL(filename.c_str(), TRUE, 0, &item));
    DWORD stream_count;
    MASSERTHR(item->GetNumberOfStreams(&stream_count));
    std::cout << "File has " << stream_count << " streams" << std::endl;
    MASSERTHR(item->SetStreamSelection(0, TRUE));
    MASSERTHR(item->SetStreamSelection(1, TRUE));
    MASSERTHR(player->SetMediaItem(item.Get()));
    MASSERTHR(player->Play());

    MSG msg = {};
    msg.message = WM_NULL;
    bool idling = true;
    while (msg.message != WM_QUIT) {
        bool have_msg;
        if (idling) {
            have_msg = PeekMessageW(&msg, nullptr, 0, 0, PM_REMOVE);
        } else {
            GetMessageW(&msg, nullptr, 0, 0);
            have_msg = true;
        }
        if (have_msg) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            idling = true;
        } else {
            idling = win->idle();
        }
    }
}

static int mfplay_main(HINSTANCE hInstance, HINSTANCE, int nCmdShow, std::string filename = fire::arg({0, "filename"})) {
    std::wstring wfilename = gio::utf8_string_to_utf16_wstring(filename);
    MASSERTHR(MFStartup(MF_VERSION, MFSTARTUP_FULL));
    mfplay(wfilename, hInstance, nCmdShow);
    MASSERTHR(MFShutdown());
    return 0;
}

GIO_WWINMAIN_HINSTANCE_FIRE_STD_NO_SPACE_ASSIGNMENT(mfplay);

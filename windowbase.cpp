#include "windowbase.h"

WindowBase::WindowBase(const wchar_t *class_name, const wchar_t *name, HINSTANCE instance) : hwnd{} {
    this->hwnd = CreateWindowExW(0, class_name, name, WS_OVERLAPPEDWINDOW,
                                 CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                                 nullptr, nullptr, instance, static_cast<LPVOID>(this));
    MASSERT(this->hwnd);
    SetWindowLongPtrW(this->hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(static_cast<WindowBase*>(this)));
}

WindowBase::~WindowBase() {
    SetWindowLongPtrW(this->hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(nullptr));
    MASSERT(DestroyWindow(this->hwnd));
}

void WindowBase::show(int nCmdShow) {
    ShowWindow(this->hwnd, nCmdShow);
}

LRESULT WindowBase::default_handle(UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg) {
    case WM_CLOSE:
        PostQuitMessage(0);
        return 0;
    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

void WindowBase::initialize() {
}

bool WindowBase::idle() {
    return false;
}

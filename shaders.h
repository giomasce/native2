
#pragma once

#include <string>
#include <vector>
#include <tuple>

#include <windows.h>

extern const std::string vertex_shader_code;
extern const std::string pixel_shader_code;
extern std::vector<unsigned char> vertex_shader_bc;
extern std::vector<unsigned char> pixel_shader_bc;

std::tuple<HRESULT, std::vector<unsigned char>, std::string> compile_shader(const std::string &source_name, const std::string &main, const std::string &target, const std::string &code);
HRESULT compile_vertex_shader();
HRESULT compile_pixel_shader();

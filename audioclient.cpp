#include <chrono>
#include <algorithm>

#include <wrl.h>
#include <mmdeviceapi.h>
#include <audioclient.h>

#include <giolib/main.h>

#include "utils.h"

const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
const IID IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);
const IID IID_IAudioClient = __uuidof(IAudioClient);
const IID IID_IAudioRenderClient = __uuidof(IAudioRenderClient);

static const float VOL = 1.0;
static const float FREQ = 440.0;

static void synth_data(BYTE *data, uint64_t &pos, UINT32 size, const WAVEFORMATEX *fmt) {
    MASSERT(fmt->wFormatTag == WAVE_FORMAT_EXTENSIBLE);
    const WAVEFORMATEXTENSIBLE *fmtex = reinterpret_cast<const WAVEFORMATEXTENSIBLE*>(fmt);
    MASSERT(IsEqualGUID(fmtex->SubFormat, KSDATAFORMAT_SUBTYPE_IEEE_FLOAT));
    MASSERT(fmt->wBitsPerSample == 32);
    float *fdata = reinterpret_cast<float*>(data);
    for (UINT32 i = 0; i < size; i++) {
        for (int j = 0; j < fmt->nChannels; j++) {
            fdata[j + i * fmt->nChannels] = VOL * cosf(float(pos + i) / fmt->nSamplesPerSec * FREQ * 6.28f);
        }
    }
    pos += size;
}

static int audioclient_sine_main(uint32_t sleep = fire::arg({"-s", "sleep time in milliseconds"}, 500),
                                 uint32_t buffer = fire::arg({"-b", "buffer time in milliseconds"}, 1000)) {
    CoInitialize(0);
    Microsoft::WRL::ComPtr<IMMDeviceEnumerator> enumerator;
    MASSERTHR(CoCreateInstance(CLSID_MMDeviceEnumerator, nullptr, CLSCTX_ALL, IID_IMMDeviceEnumerator, &enumerator));
    Microsoft::WRL::ComPtr<IMMDevice> device;
    MASSERTHR(enumerator->GetDefaultAudioEndpoint(eRender, eConsole, &device));
    Microsoft::WRL::ComPtr<IAudioClient> client;
    MASSERTHR(device->Activate(IID_IAudioClient, CLSCTX_ALL, nullptr, &client));
    WAVEFORMATEX *fmt;
    MASSERTHR(client->GetMixFormat(&fmt));
    MASSERTHR(client->Initialize(AUDCLNT_SHAREMODE_SHARED, AUDCLNT_STREAMFLAGS_EVENTCALLBACK, buffer * 10000, 0, fmt, nullptr));
    HANDLE event = CreateEventW(nullptr, FALSE, FALSE, nullptr);
    MASSERT(event);
    MASSERTHR(client->SetEventHandle(event));
    UINT32 size;
    MASSERTHR(client->GetBufferSize(&size));
    Microsoft::WRL::ComPtr<IAudioRenderClient> render;
    MASSERTHR(client->GetService(IID_IAudioRenderClient, &render));
    BYTE *data;
    MASSERTHR(render->GetBuffer(size, &data));
    uint64_t pos = 0;
    synth_data(data, pos, size, fmt);
    MASSERTHR(render->ReleaseBuffer(size, 0));
    MASSERTHR(client->Start());

    while (true) {
        const auto before_sleep = std::chrono::steady_clock::now();
        //Sleep(sleep);
        WaitForSingleObject(event, INFINITE);
        const auto after_sleep = std::chrono::steady_clock::now();
        const auto slept_for = std::chrono::duration_cast<std::chrono::milliseconds>(after_sleep - before_sleep);
        std::cout << "Slept for " << slept_for.count() << std::endl;
        UINT32 padding;
        MASSERTHR(client->GetCurrentPadding(&padding));
        UINT32 gen = std::min<UINT32>(size - padding, 500);
        std::cout << "Generating " << gen << " samples, when size is " << size << " and padding is " << padding << std::endl;
        MASSERTHR(render->GetBuffer(gen, &data));
        synth_data(data, pos, gen, fmt);
        MASSERTHR(render->ReleaseBuffer(gen, 0));
        //std::cout << "Done" << std::endl;
    }

    CoTaskMemFree(fmt);
    CoUninitialize();
    return 0;
}

GIO_WWINMAIN_FIRE_STD_NO_SPACE_ASSIGNMENT(audioclient_sine);

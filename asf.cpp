#include <fstream>

#include <stdio.h>       // Standard I/O
#include <windows.h>     // Windows headers
#include <mfapi.h>       // Media Foundation platform
#include <wmcontainer.h> // ASF interfaces
#include <Mferror.h>

#include <giolib/main.h>

#include <utils.h>

template <class T> void SafeRelease(T **ppT)
{
    if (*ppT)
    {
        (*ppT)->Release();
        *ppT = nullptr;
    }
}

// Read data from a byte stream into a media buffer.
//
// This function reads a maximum of cbMax bytes, or up to the size size of the
// buffer, whichever is smaller. If the end of the byte stream is reached, the
// actual amount of data read might be less than either of these values.
//
// To find out how much data was read, call IMFMediaBuffer::GetCurrentLength.

static HRESULT ReadFromByteStream(
    IMFByteStream *pStream,     // Pointer to the byte stream.
    IMFMediaBuffer *pBuffer,    // Pointer to the media buffer.
    DWORD cbMax                 // Maximum amount to read.
    )
{
    DWORD cbBufferMax = 0;
    DWORD cbRead = 0;
    BYTE *pData= nullptr;

    HRESULT hr = pBuffer->Lock(&pData, &cbBufferMax, nullptr);

    // Do not exceed the maximum size of the buffer.
    if (SUCCEEDED(hr))
    {
        if (cbMax > cbBufferMax)
        {
            cbMax = cbBufferMax;
        }

        // Read up to cbMax bytes.
        hr = pStream->Read(pData, cbMax, &cbRead);
    }

    // Update the size of the valid data in the buffer.
    if (SUCCEEDED(hr))
    {
        hr = pBuffer->SetCurrentLength(cbRead);
    }
    if (pData)
    {
        pBuffer->Unlock();
    }
    return hr;
}

// Read the ASF Header Object from a byte stream and return a pointer to the
// populated ASF ContentInfo object.
//
// The current read position of the byte stream must be at the start of the
// ASF Header Object.

static HRESULT CreateContentInfo(IMFByteStream *pStream,
    IMFASFContentInfo **ppContentInfo)
{
    const DWORD MIN_ASF_HEADER_SIZE = 30;

    QWORD cbHeader = 0;
    DWORD cbBuffer = 0;

    IMFASFContentInfo *pContentInfo = nullptr;
    IMFMediaBuffer *pBuffer = nullptr;

    // Create the ASF ContentInfo object.
    HRESULT hr = MFCreateASFContentInfo(&pContentInfo);

    // Read the first 30 bytes to find the total header size.

    if (SUCCEEDED(hr))
    {
        hr = MFCreateMemoryBuffer(MIN_ASF_HEADER_SIZE, &pBuffer);
    }
    if (SUCCEEDED(hr))
    {
        hr = ReadFromByteStream(pStream, pBuffer,MIN_ASF_HEADER_SIZE);
    }
    if (SUCCEEDED(hr))
    {
        hr = pContentInfo->GetHeaderSize(pBuffer, &cbHeader);
    }

    // Pass the first 30 bytes to the ContentInfo object.
    if (SUCCEEDED(hr))
    {
        hr = pContentInfo->ParseHeader(pBuffer, 0);
    }

    SafeRelease(&pBuffer);

    if (SUCCEEDED(hr))
    {
        cbBuffer = static_cast<DWORD>(cbHeader - MIN_ASF_HEADER_SIZE);

        hr = MFCreateMemoryBuffer(cbBuffer, &pBuffer);
    }

    // Read the rest of the header and finish parsing the header.
    if (SUCCEEDED(hr))
    {
        hr = ReadFromByteStream(pStream, pBuffer, cbBuffer);
    }
    if (SUCCEEDED(hr))
    {
        hr = pContentInfo->ParseHeader(pBuffer, MIN_ASF_HEADER_SIZE);
    }
    if (SUCCEEDED(hr))
    {
        // Return the pointer to the caller.
        *ppContentInfo = pContentInfo;
        (*ppContentInfo)->AddRef();
    }
    SafeRelease(&pBuffer);
    SafeRelease(&pContentInfo);
    return hr;
}

// Create and initialize the ASF splitter.

static HRESULT CreateASFSplitter (IMFASFContentInfo* pContentInfo,
    IMFASFSplitter** ppSplitter)
{
    IMFASFSplitter *pSplitter = nullptr;

    // Create the splitter object.
    HRESULT hr = MFCreateASFSplitter(&pSplitter);

    // Initialize the splitter to work with specific ASF data.
    if (SUCCEEDED(hr))
    {
        hr = pSplitter->Initialize(pContentInfo);
    }
    if (SUCCEEDED(hr))
    {
        // Return the object to the caller.
        *ppSplitter = pSplitter;
        (*ppSplitter)->AddRef();
    }
    SafeRelease(&pSplitter);
    return hr;
}

// Select the first video stream for parsing with the ASF splitter.

static HRESULT SelectVideoStream(IMFASFContentInfo *pContentInfo,
    IMFASFSplitter *pSplitter, BOOL *pbHasVideo)
{
    DWORD   cStreams = 0;
    WORD    wStreamID = 0;

    IMFASFProfile *pProfile = nullptr;
    IMFASFStreamConfig *pStream = nullptr;

    // Get the ASF profile from the ContentInfo object.
    HRESULT hr = pContentInfo->GetProfile(&pProfile);

    // Loop through all of the streams in the profile.
    if (SUCCEEDED(hr))
    {
        hr = pProfile->GetStreamCount(&cStreams);
    }

    std::vector<WORD> selected_streams;
    if (SUCCEEDED(hr))
    {
        for (DWORD i = 0; i < cStreams; i++)
        {
            GUID    streamType = GUID_NULL;

            // Get the stream type and stream identifier.
            hr = pProfile->GetStream(i, &wStreamID, &pStream);
            if (FAILED(hr))
            {
                break;
            }

            selected_streams.push_back(static_cast<WORD>(wStreamID));

            hr = pStream->GetStreamType(&streamType);
            if (FAILED(hr))
            {
                break;
            }

            if (streamType == MFMediaType_Video)
            {
                *pbHasVideo = TRUE;
                break;
            }
            SafeRelease(&pStream);
        }
    }

    // Select the video stream, if found.
    if (/* DISABLES CODE */ (false) && SUCCEEDED(hr))
    {
        if (*pbHasVideo)
        {
            // SelectStreams takes an array of stream identifiers.
            hr = pSplitter->SelectStreams(&wStreamID, 1);
        }
    } else {
        hr = pSplitter->SelectStreams(selected_streams.data(), static_cast<WORD>(selected_streams.size()));
    }
    SafeRelease(&pStream);
    SafeRelease(&pProfile);
    return hr;
}

static inline BOOL IsRandomAccessPoint(IMFSample *pSample)
{
    // Check for the "clean point" attribute. Default to FALSE.
    return static_cast<BOOL>(MFGetAttributeUINT32(pSample, MFSampleExtension_CleanPoint, FALSE));
}

static void DisplayKeyFrame(IMFSample *pSample)
{
    DWORD   cBuffers = 0;           // Buffer count
    DWORD   cbTotalLength = 0;      // Buffer length
    MFTIME  hnsTime = 0;            // Time stamp

    // Print various information about the key frame.
    if (SUCCEEDED(pSample->GetBufferCount(&cBuffers)))
    {
        wprintf_s(L"Buffer count: %d\n", cBuffers);
    }
    if (SUCCEEDED(pSample->GetTotalLength(&cbTotalLength)))
    {
        wprintf_s(L"Length: %d bytes\n", cbTotalLength);
    }
    if (SUCCEEDED(pSample->GetSampleTime(&hnsTime)))
    {
        // Convert the time stamp to seconds.
        double sec = static_cast<double>(hnsTime / 10000) / 1000;
        wprintf_s(L"Time stamp: %f sec.\n", sec);
    }
    wprintf_s(L"\n");
}

// Parse the video stream and display information about the video samples.
//
// The current read position of the byte stream must be at the start of the ASF
// Data Object.

static HRESULT DisplayKeyFrames(IMFByteStream *pStream, IMFASFSplitter *pSplitter, const std::string &output_filename)
{
    const DWORD cbReadSize = 2048;  // Read size (arbitrary value)

    std::ofstream output(output_filename.c_str(), std::ios::binary | std::ios::trunc);

    IMFMediaBuffer *pBuffer = nullptr;
    IMFSample *pSample = nullptr;

    HRESULT hr = S_OK;
    while (SUCCEEDED(hr))
    {
        // The parser must get a newly allocated buffer each time.
        hr = MFCreateMemoryBuffer(cbReadSize, &pBuffer);
        if (FAILED(hr))
        {
            break;
        }

        // Read data into the buffer.
        hr = ReadFromByteStream(pStream, pBuffer, cbReadSize);
        if (FAILED(hr))
        {
            break;
        }

        // Get the amound of data that was read.
        DWORD cbData;
        hr = pBuffer->GetCurrentLength(&cbData);
        if (FAILED(hr))
        {
            break;
        }

        if (cbData == 0)
        {
            break; // End of file.
        }

        // Send the data to the ASF splitter.
        hr = pSplitter->ParseData(pBuffer, 0, 0);
        SafeRelease(&pBuffer);
        if (FAILED(hr))
        {
            break;
        }

        // Pull samples from the splitter.
        DWORD parsingStatus = 0;
        do
        {
            WORD streamID;
            IMFMediaBuffer *buffer = nullptr;

            hr = pSplitter->GetNextSample(&parsingStatus, &streamID, &pSample);
            if (FAILED(hr))
            {
                break;
            }
            if (pSample == nullptr)
            {
                // No samples yet. Parse more data.
                break;
            }
            if (IsRandomAccessPoint(pSample))
            {
                DisplayKeyFrame(pSample);
            }
            pSample->ConvertToContiguousBuffer(&buffer);
            BYTE *buffer_ptr;
            DWORD buffer_length;
            buffer->Lock(&buffer_ptr, nullptr, &buffer_length);
            std::cout << "Got sample " << pSample << " of size " << buffer_length << " from stream " << streamID << std::endl;
            output.write(reinterpret_cast<const char*>(buffer_ptr), buffer_length);
            buffer->Unlock();
            SafeRelease(&buffer);
            SafeRelease(&pSample);

        } while (parsingStatus & ASF_STATUSFLAGS_INCOMPLETE);
    }
    SafeRelease(&pSample);
    SafeRelease(&pBuffer);
    return hr;
}

static int unpack_asf_main(std::string input_filename = fire::arg({0, "input filename"}),
                           std::string output_filename = fire::arg({1, "output filename"}))
{
    // Start the Media Foundation platform.
    HRESULT hr = MFStartup(MF_VERSION);
    if (SUCCEEDED(hr))
    {
        BOOL   bHasVideo = FALSE;

        IMFByteStream       *pStream = nullptr;
        IMFASFContentInfo   *pContentInfo = nullptr;
        IMFASFSplitter      *pSplitter = nullptr;

        // Open the file.
        hr = MFCreateFile(MF_ACCESSMODE_READ, MF_OPENMODE_FAIL_IF_NOT_EXIST,    MF_FILEFLAGS_NONE, gio::utf8_string_to_utf16_wstring(input_filename).c_str(), &pStream);

        // Read the ASF header.
        if (SUCCEEDED(hr))
        {
            hr = CreateContentInfo(pStream, &pContentInfo);
        }

        // Create the ASF splitter.
        if (SUCCEEDED(hr))
        {
            hr = CreateASFSplitter(pContentInfo, &pSplitter);
        }

        // Select the first video stream.
        if (SUCCEEDED(hr))
        {
            hr = SelectVideoStream(pContentInfo, pSplitter, &bHasVideo);
        }

        // Parse the ASF file.
        if (SUCCEEDED(hr))
        {
            if (/* DISABLES CODE */ (true))
            {
                hr = DisplayKeyFrames(pStream, pSplitter, output_filename);
            }
            else
            {
                wprintf_s(L"No video stream.\n");
            }
        }
        SafeRelease(&pSplitter);
        SafeRelease(&pContentInfo);
        SafeRelease(&pStream);

        // Shut down the Media Foundation platform.
        MFShutdown();
    }
    if (FAILED(hr))
    {
        wprintf_s(L"Error: 0x%X\n", hr);
    }
    return 0;
}

GIO_WWINMAIN_FIRE_STD_NO_SPACE_ASSIGNMENT(unpack_asf)

static int decode_wma_main(std::string input_filename = fire::arg({0, "input filename"}),
                           std::string output_filename = fire::arg({1, "output filename"})) {
    MASSERTHR(CoInitialize(nullptr));
    MASSERTHR(MFStartup(MF_VERSION));

    IMFTransform *transform;
    MASSERTHR(CoCreateInstance(CLSID_WMADecMediaObject, nullptr, CLSCTX_INPROC_SERVER, IID_IMFTransform, reinterpret_cast<void**>(&transform)));

    static const DWORD BUFFER_LENGTH = 8917;

    IMFMediaType *type;
    MASSERTHR(MFCreateMediaType(&type));
    MASSERTHR(type->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio));
    MASSERTHR(type->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_WMAudioV8));
    MASSERTHR(type->SetUINT32(MF_MT_AUDIO_BLOCK_ALIGNMENT, BUFFER_LENGTH));
    MASSERTHR(type->SetUINT32(MF_MT_AUDIO_NUM_CHANNELS, 2));
    MASSERTHR(type->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, 44100));
    MASSERTHR(type->SetUINT32(MF_MT_AUDIO_AVG_BYTES_PER_SECOND, 24000));
    if (false) {
        MASSERTHR(type->SetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, 16));
    }
    UINT8 user_data[16] = {};
    MASSERTHR(type->SetBlob(MF_MT_USER_DATA, user_data, sizeof(user_data)));
    MASSERTHR(transform->SetInputType(0, type, 0));
    SafeRelease(&type);

    MASSERTHR(MFCreateMediaType(&type));
    MASSERTHR(type->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio));
    MASSERTHR(type->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_Float));
    MASSERTHR(type->SetUINT32(MF_MT_AUDIO_BLOCK_ALIGNMENT, 8));
    MASSERTHR(type->SetUINT32(MF_MT_AUDIO_NUM_CHANNELS, 2));
    MASSERTHR(type->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, 44100));
    MASSERTHR(type->SetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, 32));
    MASSERTHR(type->SetUINT32(MF_MT_AUDIO_AVG_BYTES_PER_SECOND, 352800));
    if (false) {
        MASSERTHR(type->SetUINT32(MF_MT_ALL_SAMPLES_INDEPENDENT, 1));
        MASSERTHR(type->SetUINT32(MF_MT_FIXED_SIZE_SAMPLES, 1));
        MASSERTHR(type->SetUINT32(MF_MT_AUDIO_PREFER_WAVEFORMATEX, 1));
    }
    MASSERTHR(transform->SetOutputType(0, type, 0));
    SafeRelease(&type);

    MFT_INPUT_STREAM_INFO input_stream_info;
    MASSERTHR(transform->GetInputStreamInfo(0, &input_stream_info));
    std::cout << "Input stream info: " << input_stream_info.hnsMaxLatency << " " << std::hex << input_stream_info.dwFlags << " " << std::dec << input_stream_info.cbSize
              << " " << input_stream_info.cbMaxLookahead << " " << input_stream_info.cbAlignment << std::endl;

    MFT_OUTPUT_STREAM_INFO output_stream_info;
    MASSERTHR(transform->GetOutputStreamInfo(0, &output_stream_info));
    std::cout << "Output stream info: " << std::hex << output_stream_info.dwFlags << " " << std::dec << output_stream_info.cbSize << " " << output_stream_info.cbAlignment << std::endl;

    IMFByteStream *byte_stream;
    MASSERTHR(MFCreateFile(MF_ACCESSMODE_READ, MF_OPENMODE_FAIL_IF_NOT_EXIST, MF_FILEFLAGS_NONE, gio::utf8_string_to_utf16_wstring(input_filename).c_str(), &byte_stream));
    std::ofstream output(output_filename, std::ios::binary | std::ios::trunc);
    while (true) {
        IMFMediaBuffer *buffer;
        MASSERTHR(MFCreateMemoryBuffer(BUFFER_LENGTH, &buffer));
        MASSERTHR(ReadFromByteStream(byte_stream, buffer, BUFFER_LENGTH));
        DWORD length;
        MASSERTHR(buffer->GetCurrentLength(&length));
        if (length == 0) {
            break;
        }
        std::cout << "Feeding buffer with length " << length << std::endl;
        IMFSample *sample;
        MASSERTHR(MFCreateSample(&sample));
        MASSERTHR(sample->AddBuffer(buffer));
        MASSERTHR(transform->ProcessInput(0, sample, 0));
        SafeRelease(&sample);
        SafeRelease(&buffer);
        while (true) {
            DWORD status;
            MFT_OUTPUT_DATA_BUFFER output_sample = {};
            output_sample.dwStreamID = 0;
            MASSERTHR(MFCreateMemoryBuffer(output_stream_info.cbSize, &buffer));
            MASSERTHR(MFCreateSample(&sample));
            MASSERTHR(sample->AddBuffer(buffer));
            SafeRelease(&buffer);
            output_sample.pSample = sample;
            HRESULT hr = transform->ProcessOutput(0, 1, &output_sample, &status);
            if (hr == MF_E_TRANSFORM_NEED_MORE_INPUT) {
                std::cout << "Need more input" << std::endl;
                break;
            }
            MASSERTHR(hr);
            MASSERTHR(sample->ConvertToContiguousBuffer(&buffer));
            BYTE *buffer_ptr;
            DWORD buffer_length;
            MASSERTHR(buffer->Lock(&buffer_ptr, nullptr, &buffer_length));
            std::cout << "Got an output sample of size " << buffer_length << std::endl;
            output.write(reinterpret_cast<const char*>(buffer_ptr), buffer_length);
            MASSERTHR(buffer->Unlock());
            SafeRelease(&sample);
            SafeRelease(&buffer);
            SafeRelease(&output_sample.pEvents);
            if (!(output_sample.dwStatus & MFT_OUTPUT_DATA_BUFFER_INCOMPLETE)) {
                break;
            }
        }
    }

    SafeRelease(&byte_stream);
    SafeRelease(&transform);

    MASSERTHR(MFShutdown());
    CoUninitialize();
    return 0;
}

GIO_WWINMAIN_FIRE_STD_NO_SPACE_ASSIGNMENT(decode_wma)

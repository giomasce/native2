
all: native2_32.exe native2_64.exe

native2_32.exe: main_32.obj utils_32.obj d2dwindow_32.obj shaders_32.obj windowbase_32.obj gdiwindow_32.obj fonts_32.obj d3dwindow_32.obj patch_32.obj layeredwin_32.obj crash_32.obj
	i686-w64-mingw32-g++ -g -Og -std=c++17 -municode -msse -static-libgcc -static-libstdc++ -o $@ $^ -ld3dcompiler -ld3d11 -lwindowscodecs -ld2d1 -lole32 -ldwrite -lgdi32 -luser32 -ld3d9 -lusp10

%_32.obj: %.cpp Makefile
	i686-w64-mingw32-g++ -g -Og -std=c++17 -municode -msse -fno-omit-frame-pointer -Ilibs/glm -Ilibs/giolib -o $@ -c $<

native2_64.exe: main_64.obj utils_64.obj d2dwindow_64.obj shaders_64.obj windowbase_64.obj gdiwindow_64.obj fonts_64.obj d3dwindow_64.obj patch_64.obj layeredwin_64.obj crash_64.obj
	x86_64-w64-mingw32-g++ -g -Og -std=c++17 -municode -msse -static-libgcc -static-libstdc++ -o $@ $^ -ld3dcompiler -ld3d11 -lwindowscodecs -ld2d1 -lole32 -ldwrite -lgdi32 -luser32 -ld3d9 -lusp10

%_64.obj: %.cpp Makefile
	x86_64-w64-mingw32-g++ -g -Og -std=c++17 -municode -msse -fno-omit-frame-pointer -Ilibs/glm -Ilibs/giolib -o $@ -c $<

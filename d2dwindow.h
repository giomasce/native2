#pragma once

#include <d2d1.h>
#include <wrl.h>
#include <dwrite.h>

#include "windowbase.h"

class D2DWindow : public WindowBase {
public:
    D2DWindow(const wchar_t *class_name, const wchar_t *name, HINSTANCE instance);
    LRESULT handle(UINT uMsg, WPARAM wParam, LPARAM lParam);

private:
    HRESULT create_graphics_resources();
    void discard_grahics_resources();
    void calculate_layout();
    HRESULT resize();
    HRESULT paint();

    Microsoft::WRL::ComPtr<ID2D1Factory> d2d_factory;
    Microsoft::WRL::ComPtr<IDWriteFactory> dwrite_factory;
    Microsoft::WRL::ComPtr<IDWriteTextFormat> format;
    Microsoft::WRL::ComPtr<ID2D1HwndRenderTarget> render_target;
    Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> brush;
    Microsoft::WRL::ComPtr<ID2D1PathGeometry> path;
    Microsoft::WRL::ComPtr<ID2D1TransformedGeometry> trans_path;
    D2D1_ELLIPSE ellipse;
    D2D1_RECT_F rect;
    D2D1_ROUNDED_RECT rrect;
};

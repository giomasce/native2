#include "gdiwindow.h"

#include <synchapi.h>
#include <windowsx.h>
#include <usp10.h>
#include <richedit.h>

#include <giolib/main.h>
#include <giolib/encoding.h>
//#include <giolib/stacktrace.h>

GDIWindow::GDIWindow(const wchar_t *class_name, const wchar_t *name, HINSTANCE instance, BOOL quit_immediately, HWND msg_target) :
    WindowBase(class_name, name, instance), quit_immediately(quit_immediately), instance(instance), msg_target(msg_target) {
}

const char *dump_msg(UINT msg) {
    switch (msg) {
#define X(m) case m: return #m
    X(WM_CREATE);
    X(WM_NCCREATE);
    X(WM_NCACTIVATE);
    X(WM_NCHITTEST);
    X(WM_ACTIVATE);
    X(WM_KILLFOCUS);
    X(WM_ACTIVATEAPP);
    X(WM_USER);
    X(WM_GETICON);
    X(WM_SETCURSOR);
    X(WM_WINDOWPOSCHANGING);
    X(WM_WINDOWPOSCHANGED);
    X(WM_KEYUP);
    X(WM_KEYDOWN);
    X(WM_SYSKEYUP);
    X(WM_SYSKEYDOWN);
    X(WM_SYSCOMMAND);
    X(WM_SETFOCUS);
    X(WM_CLOSE);
    X(WM_MOUSEFIRST);
    X(WM_IME_SETCONTEXT);
    X(WM_IME_NOTIFY);
    default: return "<unk>";
    }
}

LRESULT GDIWindow::handle(UINT uMsg, WPARAM wParam, LPARAM lParam) {
    std::cout << dump_msg(uMsg) << " (" << std::hex << uMsg << ") received with " << std::hex << wParam << ", " << lParam << std::endl;
    std::cout << "InSendMessageEx: " << InSendMessageEx(nullptr) << std::endl;
    switch (uMsg) {
    case WM_PAINT:
        MASSERT(SUCCEEDED(this->paint()));
        return 0;
    case WM_SIZE:
        MASSERT(SUCCEEDED(this->resize()));
        return 0;
    case WM_ACTIVATEAPP: {
        /*std::cout << "WM_ACTIVATEAPP received with " << std::hex << wParam << ", " << lParam << std::endl;
        std::cout << "InSendMessageEx: " << InSendMessageEx(nullptr) << std::endl;
        auto stacktrace = gio::get_stacktrace();
        gio::dump_stacktrace(std::cout, stacktrace);
        std::cout << "WM_ACTIVATEAPP finished with " << std::hex << wParam << ", " << lParam << std::endl;*/
        break; }
    case WM_USER: {
        std::cout << "WM_USER received with " << std::hex << wParam << ", " << lParam << std::endl;
        std::cout << "InSendMessageEx: " << InSendMessageEx(nullptr) << std::endl;
        /*auto stacktrace = gio::get_stacktrace();
        gio::dump_stacktrace(std::cout, stacktrace);
        std::cout << "WM_USER finished with " << std::hex << wParam << ", " << lParam << std::endl;*/
        break; }
    }
    return this->default_handle(uMsg, wParam, lParam);
}

void GDIWindow::initialize() {
    LoadLibraryA("riched20.dll");
    this->edit_wnd = CreateWindowExW(0, RICHEDIT_CLASSW, L"",
                                     ES_MULTILINE | WS_VISIBLE | WS_CHILD | WS_BORDER | WS_TABSTOP,
                                     550, 10, 400, 100,
                                     this->hwnd, nullptr, this->instance, nullptr);
    SendMessageW(this->edit_wnd, WM_SETTEXT, 0, reinterpret_cast<LPARAM>(L"Rich text, inside rich edit!\nРУССКИЙ\nabc日本語def\n🍷, 👨, 👨🏻‍🦱, 🇮🇹"));
    CHARFORMATW format;
    format.cbSize = sizeof(format);
    format.dwMask = CFM_FACE;
    wcscpy(format.szFaceName, L"Arial");
    SendMessageW(this->edit_wnd, EM_SETCHARFORMAT, SCF_ALL, reinterpret_cast<LPARAM>(&format));
}

HRESULT GDIWindow::resize() {
    if (!InvalidateRect(this->hwnd, nullptr, TRUE)) return E_FAIL;
    return S_OK;
}

HRESULT GDIWindow::paint() {
    HRESULT hr;
    PAINTSTRUCT ps;

    if (this->quit_immediately) {
        std::cout << "Requesting quit" << std::endl;
        PostQuitMessage(0);
        return S_OK;
    }

    std::wcout.exceptions(std::ios::failbit | std::ios::badbit);

    if (!BeginPaint(this->hwnd, &ps)) return E_FAIL;
    if (!SelectObject(ps.hdc, GetStockObject(WHITE_BRUSH))) return E_FAIL;
    if (!Rectangle(ps.hdc, ps.rcPaint.left, ps.rcPaint.top, ps.rcPaint.right, ps.rcPaint.bottom)) return E_FAIL;
    if (!SelectObject(ps.hdc, GetStockObject(GRAY_BRUSH))) return E_FAIL;
    if (!Rectangle(ps.hdc, 50, 50, 100, 100)) return E_FAIL;
    if (!Arc(ps.hdc, 150, 50, 200, 100, 200, 50, 200, 100)) return E_FAIL;
    if (false) {
        if (!EndPaint(this->hwnd, &ps)) return E_FAIL;
        return S_OK;
    }
    if (!SelectObject(ps.hdc, GetStockObject(SYSTEM_FONT))) return E_FAIL;
    HFONT font = CreateFont(-25, 0, 0, 0, 400, 0, 0, 0, 0, 0, 0, 0, 0, L"Arial");
    SelectFont(ps.hdc, font);
    static const WCHAR *strings[] = {
        L"Hello, String!",
        L"РУССКИЙ",
        L"abc日本語def",
        L"ויקיפדיה (באנגלית: Wikipedia) רב־לשונית",
        L"🍷, 👨, 👨🏻‍🦱, 🇮🇹",
        L"العربية",
        L"δῖος Ἀχιλλεύς",
        L"देवनागरी",
        L"𐤡𐤢𐤤𐤥",
        L"᚛ᚁᚔᚃᚐᚔᚇᚑᚅᚐᚄᚋᚐᚊᚔᚋᚒᚉᚑᚔ᚜ ᚛ᚉᚒᚅᚐᚃᚐ[ᚂᚔ]᚜",
    };
    for (unsigned i = 0; i < ARRAYSIZE(strings); i++) {
        if (!ExtTextOutW(ps.hdc, 50, 150 + i*40, 0 /*ETO_RTLREADING*/ , NULL, strings[i], wcslen(strings[i]), NULL)) return E_FAIL;
    }
    SCRIPT_CACHE cache = nullptr;
    for (unsigned i = 0; i < ARRAYSIZE(strings); i++) {
        SCRIPT_ITEM items[257];
        int item_count;
        std::cout << "Processing string " << gio::utf16_wstring_to_utf8_string(strings[i]) << std::endl;
        MASSERTHR(ScriptItemize(strings[i], wcslen(strings[i]), ARRAYSIZE(items)-1, nullptr, nullptr, items, &item_count));
        std::cout << "Itemized " << item_count << " runs" << std::endl;
        int total_advance = 0;
        for (int j = 0; j < item_count; j++) {
            WORD glyphs[256];
            WORD clusters[256];
            SCRIPT_VISATTR visattrs[256];
            int glyph_count;
            if (FAILED(ScriptShape(ps.hdc, &cache, strings[i] + items[j].iCharPos, items[j+1].iCharPos - items[j].iCharPos, ARRAYSIZE(glyphs), &items[j].a, glyphs, clusters, visattrs, &glyph_count))) {
                std::cout << "Shaping failed, moving on" << std::endl;
                continue;
            }
            std::cout << "Shaped " << glyph_count << " glyphs:";
            for (int k = 0; k < glyph_count; k++) {
                std::cout << " " << glyphs[k];
            }
            std::cout << std::endl;
            GOFFSET offsets[256];
            int advances[256];
            ABC abc = {0, 0, 0};
            MASSERTHR(ScriptPlace(ps.hdc, &cache, glyphs, glyph_count, visattrs, &items[j].a, advances, offsets, &abc));
            std::cout << "Advances:";
            for (int k = 0; k < glyph_count; k++) {
                std::cout << " " << advances[k];
            }
            std::cout << std::endl;
            MASSERTHR(ScriptTextOut(ps.hdc, &cache, 550 + total_advance, 150 + i*40, 0, NULL, &items[j].a, nullptr, 0, glyphs, glyph_count, advances, nullptr, &offsets[0]));
            for (int k = 0; k < glyph_count; k++) {
                total_advance += advances[k];
            }
        }
    }
    if (!EndPaint(this->hwnd, &ps)) return E_FAIL;

    std::cout << "Sending to " << this->hwnd << std::endl;
    SendMessageW(this->hwnd, WM_USER, 0x22, 0x4);
    PostMessageW(this->hwnd, WM_USER, 0x22, 0x5);

    // new std::thread([this]() {
    //     SendMessageW(this->hwnd, WM_USER, 0x22, 0x6);
    //     PostMessageW(this->hwnd, WM_USER, 0x22, 0x7);
    // });

    std::cout << "Sending to " << this->msg_target << std::endl;
    SetLastError(0);
    SendMessageW(this->msg_target, WM_USER, 0x100, 0x200);
    auto last_error = GetLastError();
    std::cout << "Sent " << last_error << std::endl;

    return S_OK;
}

static LRESULT CALLBACK waiter_handler(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg) {
    case WM_USER:
        std::cout << "WM_USER received in target window with " << std::hex << wParam << ", " << lParam << std::endl;
        Sleep(100000);
        std::cout << "WM_USER finished in target window with " << std::hex << wParam << ", " << lParam << std::endl;
        break;
    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

void InitializeConditionVariable(PCONDITION_VARIABLE ConditionVariable);
void WakeAllConditionVariable(PCONDITION_VARIABLE ConditionVariable);
BOOL SleepConditionVariableCS(PCONDITION_VARIABLE ConditionVariable, PCRITICAL_SECTION CriticalSection, DWORD dwMilliseconds);

struct thread_data {
    CRITICAL_SECTION cs;
    CONDITION_VARIABLE cv;
    HINSTANCE hInstance;
    HWND msg_target;
};

static DWORD WINAPI thread_proc(LPVOID lParam) {
    thread_data *data = (thread_data *)lParam;
    WNDCLASS wc = {};
    wc.lpfnWndProc = waiter_handler;
    wc.hInstance = data->hInstance;
    wc.lpszClassName = L"waiter";
    auto atom = RegisterClassW(&wc);
    MASSERT(atom);
    EnterCriticalSection(&data->cs);
    data->msg_target = CreateWindowW(L"waiter", L"waiter", WS_OVERLAPPED, 0, 0, 0, 0, HWND_MESSAGE, nullptr, data->hInstance, nullptr);
    MASSERT(data->msg_target);
    LeaveCriticalSection(&data->cs);
    WakeAllConditionVariable(&data->cv);
    MSG msg = {};
    while (msg.message != WM_QUIT) {
        std::cout << "Target GetMessage" << std::endl;
        GetMessageW(&msg, nullptr, 0, 0);
        std::cout << "Target got message" << std::endl;
        TranslateMessage(&msg);
        DispatchMessageW(&msg);
    }
    std::cout << "Target exits" << std::endl;
    return 0;
}

GIO_WWINMAIN(gdi) {
    thread_data data;
    InitializeCriticalSection(&data.cs);
    InitializeConditionVariable(&data.cv);
    data.msg_target = 0;
    HANDLE thread = CreateThread(nullptr, 0, thread_proc, &data, 0, nullptr);
    EnterCriticalSection(&data.cs);
    while (data.msg_target == 0) {
        SleepConditionVariableCS(&data.cv, &data.cs, INFINITE);
    }
    LeaveCriticalSection(&data.cs);
    std::cout << "Target window is " << data.msg_target << std::endl;
    return native2_wWinMain<GDIWindow>(hInstance, hPrevInstance, pCmdLine, nCmdShow, FALSE, data.msg_target);
}

GIO_WWINMAIN(gdi_quit_immediately) {
    std::cout << "Starting main" << std::endl;
    return native2_wWinMain<GDIWindow>(hInstance, hPrevInstance, pCmdLine, nCmdShow, TRUE);
}

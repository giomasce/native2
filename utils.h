#pragma once

#include <windows.h>

void failed_assertion(const char *file, int line_num, const char *cond);
void failed_assertion_hr(const char *file, int line_num, HRESULT hr, const char *cond);

#define MASSERT(cond) if (!(cond)) { failed_assertion(__FILE__, __LINE__, #cond); } (void) 0
#define MASSERTHR(hr) if (FAILED(hr)) { failed_assertion_hr(__FILE__, __LINE__, hr, #hr); } (void) 0

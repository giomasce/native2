#include "utils.h"

#include <sstream>
#include <iostream>

#include <comdef.h>

void fatal_exit(const WCHAR *msg) {
    //FatalAppExitW(0, msg);
    std::wcerr << msg << std::endl;
    exit(1);
}

void failed_assertion(const char *file, int line_num, const char *cond) {
    std::wostringstream oss;
    auto error_code = GetLastError();
    wchar_t *error_msg;
    FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                   nullptr, error_code, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), reinterpret_cast<LPTSTR>(&error_msg), 0, nullptr);
    auto len = wcslen(error_msg);
    if (len >= 1 && error_msg[len-1] == '\n') {
        error_msg[len-1] = '\0';
    }
    if (len >= 2 && error_msg[len-2] == '\r') {
        error_msg[len-2] = '\0';
    }
    oss << "Failed assertion '" << cond << "' at " << file << ':' << line_num << " with error '" << error_msg << "'";
    LocalFree(error_msg);
    auto msg = oss.str();
    fatal_exit(msg.c_str());
}

DWORD Win32FromHResult(HRESULT hr) {
    if ((hr & 0xFFFF0000) == MAKE_HRESULT(SEVERITY_ERROR, FACILITY_WIN32, 0)) {
        return HRESULT_CODE(hr);
    }

    if (hr == S_OK) {
        return ERROR_SUCCESS;
    }

    // Not a Win32 HRESULT so return a generic error code.
    return ERROR_CAN_NOT_COMPLETE;
}

void failed_assertion_hr(const char *file, int line_num, HRESULT hr, const char *cond) {
    std::wostringstream oss;
    auto error_code = Win32FromHResult(hr);
    wchar_t *error_msg;
    FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                   nullptr, error_code, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), reinterpret_cast<LPTSTR>(&error_msg), 0, nullptr);
    _com_error err(hr);
    LPCTSTR errMsg = err.ErrorMessage();
    auto len = wcslen(error_msg);
    if (len >= 1 && error_msg[len-1] == '\n') {
        error_msg[len-1] = '\0';
    }
    if (len >= 2 && error_msg[len-2] == '\r') {
        error_msg[len-2] = '\0';
    }
    oss << "Failure HRESULT " << std::hex << hr << std::dec << " received at " << file << ':' << line_num << " with error '" << errMsg << "' at expression " << cond;
    LocalFree(error_msg);
    auto msg = oss.str();
    fatal_exit(msg.c_str());
}
